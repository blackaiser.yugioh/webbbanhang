-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 29, 2023 lúc 06:38 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `kltn`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `area`
--

CREATE TABLE `area` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_code` tinyint(4) DEFAULT NULL,
  `district_code` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `hide` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0: ẩn, 1 hiển thị'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `code`, `thumbnail`, `parent`, `created_at`, `updated_at`, `deleted_at`, `hide`) VALUES
(1, 'Laptop', '011', '/storage/category/TWChNJYEWiZ3GzBC4BqA.jpg', 0, NULL, '2023-07-26 02:16:16', NULL, 0),
(2, 'Dell', '002', '/storage/category/TWChNJYEWiZ3GzBC4BqA.jpg', 1, NULL, NULL, NULL, 1),
(3, 'Máy tính bảng', '012', '/storage/category/OqrYcTs3IYfT6LNDZzwp.jpg', 0, NULL, NULL, NULL, 1),
(4, 'Điện thoại', 'Bill1639738347', '/storage/category/TWChNJYEWiZ3GzBC4BqA.jpg', 0, NULL, '2023-07-06 03:06:31', NULL, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `city`
--

CREATE TABLE `city` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_code` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `city`
--

INSERT INTO `city` (`id`, `name`, `city_code`, `created_at`, `updated_at`) VALUES
(1, 'Hà Nội', 1, NULL, NULL),
(2, 'Hồ Chí Minh', 2, NULL, NULL),
(3, 'An Giang', 56, NULL, NULL),
(4, 'Bà Rịa - Vũng Tàu', 53, NULL, NULL),
(5, 'Bắc Ninh', 11, NULL, NULL),
(6, 'Bắc Giang', 27, NULL, NULL),
(7, 'Bình Dương', 50, NULL, NULL),
(8, 'Bình Định', 40, NULL, NULL),
(9, 'Bình Phước', 48, NULL, NULL),
(10, 'Bình Thuận', 52, NULL, NULL),
(11, 'Bến Tre', 58, NULL, NULL),
(12, 'Bắc Kạn', 21, NULL, NULL),
(13, 'Cần Thơ', 5, NULL, NULL),
(14, 'Khánh Hòa', 42, NULL, NULL),
(15, 'Thừa Thiên Huế', 37, NULL, NULL),
(16, 'Lào Cai', 20, NULL, NULL),
(17, 'Quảng Ninh', 28, NULL, NULL),
(18, 'Đồng Nai', 51, NULL, NULL),
(19, 'Nam Định', 15, NULL, NULL),
(20, 'Cà Mau', 63, NULL, NULL),
(21, 'Cao Bằng', 19, NULL, NULL),
(22, 'Gia Lai', 44, NULL, NULL),
(23, 'Hà Giang', 18, NULL, NULL),
(24, 'Hà Nam', 14, NULL, NULL),
(25, 'Hà Tĩnh', 34, NULL, NULL),
(26, 'Hải Dương', 12, NULL, NULL),
(27, 'Hải Phòng', 3, NULL, NULL),
(28, 'Hòa Bình', 31, NULL, NULL),
(29, 'Hưng Yên', 13, NULL, NULL),
(30, 'Kiên Giang', 59, NULL, NULL),
(31, 'Kon Tum', 43, NULL, NULL),
(32, 'Lai Châu', 29, NULL, NULL),
(33, 'Lâm Đồng', 46, NULL, NULL),
(34, 'Lạng Sơn', 22, NULL, NULL),
(35, 'Long An', 54, NULL, NULL),
(36, 'Nghệ An', 33, NULL, NULL),
(37, 'Ninh Bình', 17, NULL, NULL),
(38, 'Ninh Thuận', 47, NULL, NULL),
(39, 'Phú Thọ', 26, NULL, NULL),
(40, 'Phú Yên', 41, NULL, NULL),
(41, 'Quảng Bình', 35, NULL, NULL),
(42, 'Quảng Nam', 38, NULL, NULL),
(43, 'Quảng Ngãi', 39, NULL, NULL),
(44, 'Quảng Trị', 36, NULL, NULL),
(45, 'Sóc Trăng', 61, NULL, NULL),
(46, 'Sơn La', 30, NULL, NULL),
(47, 'Tây Ninh', 49, NULL, NULL),
(48, 'Thái Bình', 16, NULL, NULL),
(49, 'Thái Nguyên', 25, NULL, NULL),
(50, 'Thanh Hóa', 32, NULL, NULL),
(51, 'Tiền Giang', 6, NULL, NULL),
(52, 'Trà Vinh', 60, NULL, NULL),
(53, 'Tuyên Quang', 23, NULL, NULL),
(54, 'Vĩnh Long', 57, NULL, NULL),
(55, 'Vĩnh Phúc', 10, NULL, NULL),
(56, 'Yên Bái', 24, NULL, NULL),
(57, 'Đắk Lắk', 45, NULL, NULL),
(58, 'Đồng Tháp', 55, NULL, NULL),
(59, 'Đà Nẵng', 4, NULL, NULL),
(60, 'Đắk Nông', 9, NULL, NULL),
(61, 'Hậu Giang', 8, NULL, NULL),
(62, 'Bạc Liêu', 62, NULL, NULL),
(63, 'Điện Biên', 64, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `district`
--

CREATE TABLE `district` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_code` tinyint(4) DEFAULT NULL,
  `district_code` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2023_07_06_024751_create_category_table', 2),
(7, '2023_07_06_033535_add_col_hide_category', 3),
(8, '2023_07_06_093802_create_products_table', 4),
(9, '2023_07_06_100124_create_product_images_table', 4),
(11, '2023_07_06_103556_add_slug_product', 6),
(13, '2023_07_11_024916_create_permission_tables', 7),
(14, '2023_07_11_035700_add_col_avatar', 8),
(15, '2023_07_11_044335_create_table_warehouse', 9),
(22, '2023_07_14_030508_create_voucher', 10),
(23, '2023_07_14_034304_add_col_code', 10),
(24, '2023_07_14_034931_add_col_status', 10),
(25, '2023_09_29_035245_create_city', 11),
(26, '2023_09_29_035332_create_district_table', 11),
(27, '2023_09_29_035402_create_area_table', 11);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(2, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: ẩn, 1 hiển thị',
  `quantity` int(11) NOT NULL DEFAULT 0,
  `buycnt` int(11) NOT NULL DEFAULT 0,
  `price` double(10,2) NOT NULL DEFAULT 0.00,
  `sale` int(11) NOT NULL DEFAULT 0,
  `short` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `point` double(2,1) NOT NULL DEFAULT 5.0,
  `review` tinyint(4) NOT NULL DEFAULT 5,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `slug`, `thumbnail`, `status`, `quantity`, `buycnt`, `price`, `sale`, `short`, `description`, `point`, `review`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Lenovo IdeaPad 3 15ITL6 I5 1155G7 (82H803RWVN)', 'lenovo-ideapad-3-15itl6-i5-1155g7-82h803rwvn', '/storage/products/DEJGxIX6SE7GpQbzjDm3.jpg', 1, 15, 1, 1500000.00, 5, '<p>15.6 inch, 1920 x 1080 Pixels, IPS, 60 Hz, 300 nits, Anti - Glare</p><p>Intel, Core i5, 1155G7</p><p>16 GB (2 thanh 8 GB), DDR4, 3200 MHz</p><p>SSD 512 GB</p><p>Intel Iris Xe Graphics</p>', '<p>Lenovo IdeaPad 3 15ITL6 sở hữu cấu hình lý tưởng cho công việc với bộ vi xử lý Intel Core i5 mạnh mẽ, 16GB RAM đa nhiệm hoàn hảo và SSD dung lượng lên tới 512GB. Bạn sẽ được làm việc và giải trí một cách trực quan trên màn hình lớn 15,6 inch Full HD sắc nét nhưng vẫn đảm bảo tính di động.</p>', 4.5, 0, '2023-07-10 01:50:02', '2023-09-10 21:21:57', NULL),
(2, 1, 'Asus X541UA', 'asus-x541ua', '/storage/products/qHHGzlBtWtGzboPN8JY1.jpg', 1, 51, 1, 16000000.00, 10, '<p>15.6 inch, 1920 x 1080 Pixels, IPS, 60 Hz, 300 nits, Anti - Glare</p><p>Intel, Core i5, 1155G7</p><p>16 GB (2 thanh 8 GB), DDR4, 3200 MHz</p><p>SSD 512 GB</p><p>Intel Iris Xe Graphics</p>', '<p><strong>Lenovo IdeaPad 3 15ITL6 sở hữu cấu hình lý tưởng cho công việc với bộ vi xử lý Intel Core i5 mạnh mẽ, 16GB RAM đa nhiệm hoàn hảo và SSD dung lượng lên tới 512GB. Bạn sẽ được làm việc và giải trí một cách trực quan trên màn hình lớn 15,6 inch Full HD sắc nét nhưng vẫn đảm bảo tính di động.</strong></p>', 5.0, 0, '2023-07-10 01:51:38', '2023-07-10 01:51:38', NULL),
(3, 1, 'Asus X541UAA', 'asus-x541uaa', '/storage/products/qdSJBtNuMjDgON4n29FB.jpg', 1, 51, 0, 16000000.00, 20, '<p>15.6 inch, 1920 x 1080 Pixels, IPS, 60 Hz, 300 nits, Anti - Glare</p><p>Intel, Core i5, 1155G7</p><p>16 GB (2 thanh 8 GB), DDR4, 3200 MHz</p><p>SSD 512 GB</p><p>Intel Iris Xe Graphics</p>', '<p>Lenovo IdeaPad 3 15ITL6 sở hữu cấu hình lý tưởng cho công việc với bộ vi xử lý Intel Core i5 mạnh mẽ, 16GB RAM đa nhiệm hoàn hảo và SSD dung lượng lên tới 512GB. Bạn sẽ được làm việc và giải trí một cách trực quan trên màn hình lớn 15,6 inch Full HD sắc nét nhưng vẫn đảm bảo tính di động.</p>', 5.0, 0, '2023-07-10 01:53:27', '2023-07-10 01:53:27', NULL),
(4, 1, 'Asus X541UAAa', 'asus-x541uaaa', '/storage/products/DwXW2h4s03NG1h3fg1ZB.jpg', 1, 51, 0, 16000000.00, 40, '<p>15.6 inch, 1920 x 1080 Pixels, IPS, 60 Hz, 300 nits, Anti - Glare</p><p>Intel, Core i5, 1155G7</p><p>16 GB (2 thanh 8 GB), DDR4, 3200 MHz</p><p>SSD 512 GB</p><p>Intel Iris Xe Graphics</p>', '<p>Lenovo IdeaPad 3 15ITL6 sở hữu cấu hình lý tưởng cho công việc với bộ vi xử lý Intel Core i5 mạnh mẽ, 16GB RAM đa nhiệm hoàn hảo và SSD dung lượng lên tới 512GB. Bạn sẽ được làm việc và giải trí một cách trực quan trên màn hình lớn 15,6 inch Full HD sắc nét nhưng vẫn đảm bảo tính di động.</p>', 3.6, 0, '2023-07-10 01:54:05', '2023-07-10 01:54:05', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` tinyint(4) NOT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `url`, `created_at`, `updated_at`) VALUES
(1, 4, '/storage/products/OYhKSZsHDh6tTvfOEIzm.png', NULL, NULL),
(2, 4, '/storage/products/ys9mmpfftsxzJQvqCvku.png', NULL, NULL),
(3, 1, '/storage/products/BTLB0nKkfxxCFCYe2rRF.png', NULL, NULL),
(4, 1, '/storage/products/u8qQ756suiuRvAhr5trR.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(2, 'super_admin', 'web', '2023-07-10 20:47:33', '2023-07-10 20:47:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lock` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0: khóa, 1: bình thường'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `avatar`, `lock`) VALUES
(1, 'Nguyễn Trường An', 'baomai1001001@gmail.com', NULL, '$2a$12$fX89u1mcd/Rq84O6S5R2iecoJ867ptU8nzZFJcpdyfeU0RB4ZGi5.', NULL, '2023-07-04 21:48:48', '2023-07-10 21:18:19', '/assets/img/avatars/1.png', 1),
(3, 'admin', 'admin@gmail.com', NULL, '$2y$10$6eqIA6cpkNgMhn0DJX7DyO1ENdBy991yv9RQk5ARoHbv1jz1Z7WI6', NULL, '2023-07-10 20:47:34', '2023-07-10 20:47:34', '/assets/img/avatars/1.png', 1),
(4, 'Nguyễn Trường An 01', 'baoma01@gmail.com', NULL, '$2y$10$FOKmmmK5PHstvtqPKelHZumnHnymG8sc3KjoswvUtfFHAygcDqypi', NULL, '2023-07-04 21:48:48', '2023-07-26 00:28:45', '/assets/img/avatars/1.png', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `voucher`
--

CREATE TABLE `voucher` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discount` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `voucher`
--

INSERT INTO `voucher` (`id`, `discount`, `name`, `description`, `type`, `start`, `end`, `created_at`, `updated_at`, `code`, `status`, `deleted_at`) VALUES
(1, 50000, 'giảm giá 50k', 'Giảm giá 50k trên tất cả các đơn hàng', 2, '2023-07-27 14:30:00', '2023-07-30 14:30:00', '2023-07-26 01:53:52', '2023-07-26 02:42:48', 'GG50k', 1, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `warehouse`
--

CREATE TABLE `warehouse` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Chỉ mục cho bảng `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_index` (`category_id`);

--
-- Chỉ mục cho bảng `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Chỉ mục cho bảng `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `area`
--
ALTER TABLE `area`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `city`
--
ALTER TABLE `city`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT cho bảng `district`
--
ALTER TABLE `district`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
