<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Review extends Model
{
    use HasFactory;
    public $table = 'reviews';
    public $primaryKey = 'id';
    public $fillable = [
      'user_id','name','product_id','type', 'status','phone' ,'email',
      'comment', 'star', 'reply', 'order_id'
    ];

    protected $attributes = [
        'star' => 5,
        'user_id' => 0,
        'type' => 0,
        'status' => 0,
        'reply' => 0
    ];
    // public function getCreatedAtAttribute($value)
    // {
    //     $date = Carbon::parse($value);
    //     return $date->format('d-m-Y');
    // }

    function product(){
        return $this->hasOne(Products::class,'id', 'product_id');
    }

    function user(){
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
