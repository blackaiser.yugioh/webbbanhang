<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Products;

class Cart extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'carts';
    public $primaryKey = 'id';
    public $fillable = [
        'user_id','product_id','quantity'
    ];

    public function products(){
        return $this->belongsTo(Products::class,'product_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
