<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Products;

class OrderProducts extends Model
{
    use HasFactory;
    protected $table = 'order_products';
    public $fillable = [
        'id', 'order_id', 'product_id', 'quantity', 'product_name',
        'thumbnail', 'price', 'pay_price' ,'reviewed'
    ];

    public function product(){
        return $this->hasOne(Products::class, 'id', 'product_id');
    }
}
