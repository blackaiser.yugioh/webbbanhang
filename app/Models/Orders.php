<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Orders extends Model
{
    use HasFactory;
    protected $table = 'orders';
    public $fillable = [
        'id', 'user_id', 'address', 'phone', 'total', 'code',
        'total_pay', 'ship', 'sale', 'payment_type', 'status','voucher_discount', 'voucher_id'
    ];
    // public $timestamps = false;
    public function orderProducts(){
        return $this->hasMany(OrderProducts::class, 'order_id');
    }
    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}