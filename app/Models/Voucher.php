<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    public $table = 'voucher';
    public $primaryKey = 'id';
    public $fillable = [
      'name','code','description','discount', 'min', 'start', 'end'
    ];

    protected $attributes = [
      'status' => 1,
      'start' => 0,
      'end' => 0
    ];

    protected $appends = [
      'expires'
    ];

    function getExpiresAttribute(){
      $time = time();
      if($this->end > 0 && $time > $this->end) {
        return 0;
      }
      return 1;
    }
}
