<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherUser extends Model
{
    use HasFactory;

    public $table = 'voucher_user';
    public $primaryKey = 'id';
    public $fillable = [
      'user_id','voucher_id'
    ];
}
