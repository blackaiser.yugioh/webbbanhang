<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'category';
    public $primaryKey = 'id';
    public $fillable = [
      'name','code','thumbnail','parent', 'hide'
    ];

    protected $attributes = [
        'parent' => 0,
        'hide' => 1
    ];

    public function child()
    {
        return $this->hasMany(Category::class,'parent');
    }
  
    public function getParent(){
      return $this->hasOne(Category::class, 'id', 'parent');
    }

    public function getProducts() {
      return $this->hasMany(Products::class,'category_id');
    }
}