<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Category;

class Products extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'products';
    public $primaryKey = 'id';
    public $fillable = [
      'category_id','name','thumbnail','status', 'quantity','price' ,'buycnt',
      'sale', 'short', 'description', 'point', 'review', 'slug'
    ];

    protected $attributes = [
        'buycnt' => 0,
        'sale' => 0,
        'point' => 5,
        'review' => 0,
        'status' => 1
    ];

    public function category(){
      return $this->hasOne(Category::class,'id', 'category_id');
    }

    public function images(){
      return $this->hasMany(ProductImages::class, 'product_id');
    }
}