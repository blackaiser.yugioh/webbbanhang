<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;
    protected $table = 'posts';
    public $fillable = [
        'user_id', 'image', 'title', 'description', 'status' ,'slug'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
