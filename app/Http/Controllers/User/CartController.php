<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    private $cart;
    private $userId;
    function __construct(Cart $cart) {
        $this->cart = $cart;
        $this->userId = Auth::id();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = $this->cart->where('user_id', Auth::id())->with('products')->get();
        return view('user.cart', compact('carts'));
    }

    function addToCart(Request $request) {
        $this->validate($request, [
            'product_id' => 'required|numeric',
            'quantity' => 'required|min:1|numeric',
        ]);
        $input = $request->only([
            'product_id','quantity'
        ]);

        if(!Auth::check()) {
            return json_encode([
                'status' => 401,
                'message' => __('Authenticate'),
            ]);
        }

        $input['user_id'] = Auth::id();
        $myCart = $this->cart
            ->where('product_id', $input['product_id'])
            ->where('user_id', $input['user_id'])
            ->first();
        if($myCart) {
            return json_encode([
                'status' => 200,
                'message' => __('Success'),
            ]);
        }
        $this->cart->create($input);
        return json_encode([
            'status' => 200,
            'message' => __('Success'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric'
        ]);
        $id = $request->get('id');
        $cart = $this->cart->find($id);
        if($cart && $cart->user_id === Auth::id()) {
            $cart->delete();
            return json_encode([
                'status' => 200,
                'message' => __('Success'),
            ]);
        }

        return json_encode([
            'status' => 404,
            'message' => __('Error'),
        ]);
        
    }
}
