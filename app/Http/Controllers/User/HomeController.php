<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Category;
use App\Models\Contact;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Review;

class HomeController extends Controller
{
    private $category;
    private $product;
    private $reviews;
    private $cart;

    function __construct(
        Category $category,
        Products $product,
        Review $reviews,
        Cart $cart
    ) {
        $this->category = $category;
        $this->product = $product;
        $this->reviews = $reviews;
        $this->cart = $cart;
    }

    public function index() {
        $cats = $this->category->where('hide',1)->where('parent',0)->get();
        $sales = $this->product->where('sale','>',0)->where('status', 1)->limit(10)->get();
        $bestseller = $this->product->where('status', 1)->orderBy('buycnt')->limit(10)->get();
        return view('user.index', compact('cats','sales','bestseller'));
    }

    public function detailProduct($slug){
        $product = $this->product->where('slug',$slug)->with('category', 'images')->first();
        $productSamilar = $this->product->where('category_id', $product->category->id)
            ->where('status', 1)
            ->where('quantity','>',0)
            ->whereNotIn('id',[$product->id])
            ->orderBy('buycnt','DESC')
            ->limit(5)->get();
        if(count($productSamilar) === 0) {
            $productSamilar = $this->product->where('status', 1)
            ->where('quantity','>',0)
            ->whereNotIn('id',[$product->id])
            ->orderBy('buycnt','DESC')
            ->limit(5)->get();
        }
        $query = Review::where('product_id',$product->id)->get();

        $totalStart = $query->count();
        $star = $query->groupBy('star');

        $listStar = [];
        foreach([1,2,3,4,5] as $num){
            $listStar[] = [
                !empty($star[$num]) ? (number_format(count($star[$num]) / $totalStart,2)) * 100 : 0
            ];
        }
        
        return view('user.product-detail', compact('product','productSamilar', 'listStar'));
    }
    public function review(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:50',
            'product_id' => 'required',
            'phone' => 'required|max:20',
            'email' => 'required|max:50|email',
            'comment' => 'required',
            'star' => 'required|integer|digits_between:1,5'
        ]);
        $input = $request->only([
            'name','product_id','type', 'status','phone' ,'email', 'comment', 'star', 'reply'
        ]);
        Auth::id() ? $input['user_id'] = Auth::id() : '' ;

        if($input['type'] == 1) {
            $input['status'] = 1;
            $input['order_id'] = $request->get('order_id');
        }
        $this->reviews->create($input);
        
        if($input['type'] == 1) {
            $productId = $input['product_id'];
            $product = Products::find($productId);
            $listReview = Review::select('star')->where('product_id', $productId)->where('status',1)->get();

            $total = count($listReview);
            $totalPoint = $listReview->sum('star');

            $product->update([
                'point'=>number_format($totalPoint/$total,1),
                'review' => $product->review + 1
            ]);

            OrderProducts::find($request->get('order_product'))->update(['reviewed' => 1]);

        }
        return json_encode([
            'status' => 200,
            'data' => $input['product_id'],
            'message' => __('Success'),
        ]);
    }

    public function getReview($productId){
        $reviews = $this->reviews->where('product_id', $productId)
            ->with([
                'user' => function($q){
                    $q->select('name','id');
                },
                'product' => function($qq){
                    $qq->select('name','id');
                }
            ])
            ->where('status',1)
            ->paginate(5);

        return json_encode([
            'status' => 200,
            'data' => $reviews,
            'message' => __('Success'),
        ]);
    }

    public function about() {
        return view('user.about');
    }

    public function contact() {
        return view('user.contact');
    }

    public function sendContact(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:100',
            'email' => 'required|max:50',
            'phone' => 'required|max:12',
            'message' => 'required',
        ]);

        $input = $request->only('name', 'email','phone', 'message');

        $input['user_id'] = Auth::id() ?? null;

        $contact = Contact::create($input);

        return json_encode([
            'status' => 200,
            'data' => $contact,
            'message' => __('Success'),
        ]);
    }
}
