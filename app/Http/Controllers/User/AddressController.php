<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\City;
use App\Models\District;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    private $city;
    private $district;
    private $area;

    public function __construct(
        City        $city,
        District    $district,
        Area        $area
    ) {
        $this->city     = $city;
        $this->district = $district;
        $this->area     = $area;
    }

    public function listCity() {
        $citys = $this->city->select(['id','name','city_code'])->get();

        return json_encode([
            'status' => 200,
            'data' => $citys,
            'message' => __('Success'),
        ]);
    }

    public function listDistrict(Request $request) {
        $this->validate($request, [
            'city_code' => 'numeric'
        ]);

        $cityCode = $request->get('city_code', '');
        $query = $this->district->select(['id','name','city_code','district_code']);
        if($cityCode) {
            $query->where('city_code', $cityCode);
        }
        $districts = $query->get();
        return json_encode([
            'status' => 200,
            'data' => $districts,
            'message' => $cityCode,
        ]);
    }

    public function listArea(Request $request) {
        $this->validate($request, [
            'district_code' => 'string'
        ]);

        $districtCode = $request->get('district_code', '');
        $query = $this->area->select(['id','name','district_code']);
        if($districtCode) {
            $query->where('district_code', $districtCode);
        }
        $areas = $query->get();
        return json_encode([
            'status' => 200,
            'data' => $areas,
            'message' => __('Success'),
        ]);
    }
}
