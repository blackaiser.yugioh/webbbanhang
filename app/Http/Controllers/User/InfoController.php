<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class InfoController extends Controller
{
    public function info() {
        $user = Auth::user();       
        return view('user.info', compact('user'));
    }

    public function listOrder(Request $request){
        $this->validate($request,[
            'status' => 'numeric'
        ]);
        $status = $request->get('status',10);

        $query = Orders::where('user_id', Auth::id());
        switch ($status) {
            case 10:
                $query = $query;
                break;
            case 1:
                $query = $query->whereIn('status',[1,2]);
                break;
            case 4:
                $query = $query->whereIn('status',[4,6]);
                break;
            default:
                $query = $query->where('status',$status);
                break;
        }
        $orders = $query->with('orderProducts')->orderBy('id', 'desc')->get()->each(function($q){
                $q->status_des = config('constants.order_status')[$q->status] ?? '';
                $q->create_date = date_format($q->created_at,'d/m/Y');
            });

        return json_encode([
            'status' => 200,
            'data' => $orders,
            'a'=> $status,
            'message' => __('Success'),
        ]);
    }

    public function updateUser(Request $request){
        $user = User::find(Auth::id());
        $this->validate($request, [
            'email'     => 'required|unique:users,email,'.$user->id,
            'name'      => 'required|string|max:50',
            'phone'     => 'required|unique:users,phone,'.$user->id .'|max:11|min:10|regex:/^0\d{9}$/',
            'address'   => 'string|max:100'
        ]);

        $input = $request->only('email', 'name', 'phone', 'address');

        $user->update($input);

        return json_encode([
            'status' => 200,
            'message' => __('Success'),
        ]);
    }

    public function changePassword(Request $request){
        $user = User::find(Auth::id());
        $request->validate([
            'passwordOld' => 'required',
            'passwordNew' => 'required|min:8|regex:/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]).{8,}$/',
        ],[
            'passwordNew.min' => 'Mật khẩu mới tối thiểu 8 ký tự',
            'passwordNew.regex' => 'Mật khẩu phải chứa chữ in hoa, chữ thường, số và kí tự',
        ]);
        $currentPasswordStatus = Hash::check($request->passwordOld, auth()->user()->password);

        if($currentPasswordStatus){

            User::findOrFail(Auth::user()->id)->update([
                'password' => Hash::make($request->passwordNew),
            ]);

            return json_encode([
                'status' => 200,
                'currentPasswordStatus' => $currentPasswordStatus,
                'message' => __('Success'),
            ]);

        }else{
            return json_encode([
                'status' => 1005,
                'message' => __('Success'),
            ]);
        }
    }

    public function cancelOrder($id){
        $userId = Auth::id();

        $order = Orders::where('id',$id)->where('user_id',$userId)->firstOrFail();
        if(in_array($order->status,[0,1,7])) {
            $products = OrderProducts::where('order_id', $order->id)->get();
            $order->update(['status' => 4]);
            foreach($products as $product) {
                $prod = Products::find($product->product_id);
                $prod->increment('quantity',$product->quantity);
                $prod->decrement('buycnt',$product->quantity);;
            }
            return json_encode([
                'status' => 200,
                'message' => __('Success'),
                'data' => $order
            ]);
        }
        return json_encode([
            'status' => 400,
            'message' => __('Error'),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
