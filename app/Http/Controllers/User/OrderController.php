<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\Products;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\VoucherUser;
use App\Models\Voucher;

class OrderController extends Controller
{
    private $order;
    function __construct(
        Orders $order
    ) {
        $this->order = $order;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $time = time();
        $carts = Session::get('checkout');
        $user = Auth::user();
        $userVoucher = VoucherUser::where('user_id', Auth::id())->get()->pluck('voucher_id');

        $voucher = Voucher::where('status',1)->where('start', '>' , $time)->whereNotIn('id',$userVoucher)->get();
        $sort =  collect($voucher)->sortByDesc('expires');
        $voucher = $sort->values()->all();
        return view('user.checkout', compact('carts', 'user', 'voucher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = json_decode($request->get('data'));
        $productId = [];
        foreach($data as $cart) {
            array_push($productId, $cart->products->id);
        }
        $checkQuantity = Products::select('id','quantity')->whereIn('id', $productId)->get()->keyBy('id');

        foreach($data as $cart) {
            if($cart->quantity > $checkQuantity[$cart->products->id]->quantity) {
                return json_encode([
                    'status' => 400,
                    'message' => __('Error'),
                ]);
            }
        }

        $data = json_decode($request->get('data'));
        Session::forget('checkout');
        Session::put('checkout', $data);
        return json_encode([
            'status' => 200,
            'message' => __('Success'),
        ]);
    }

    public function vnPay($request)
    {
        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://localhost:3000/chi-tiet-don-hang";
        $vnp_TmnCode = "E5H8KB3B";//Mã website tại VNPAY
        $vnp_HashSecret = "JSXQQCIKLBSVEWJIRRGZDFZTFZTFHKTC"; //Chuỗi bí mật

        $vnp_TxnRef = 'OR' . time();;
        $vnp_OrderInfo = "Thanh toán đơn hàng OR" . time();
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $request['total_pay'] * 100;
        $vnp_Locale = 'vn';
        $vnp_BankCode = "NCB";
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];
        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        if (isset($vnp_Bill_State) && $vnp_Bill_State != "") {
            $inputData['vnp_Bill_State'] = $vnp_Bill_State;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash = hash_hmac('sha512', $hashdata, $vnp_HashSecret);//
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array('code' => '00'
        , 'message' => 'success'
        , 'data' => $vnp_Url);
        if (isset($_POST['redirect'])) {
            header('Location: ' . $vnp_Url);
            die();
        } else {
            echo json_encode($returnData);
        }
    }

    public function returnUrl(Request $request) {
        $orderCode = $request->get('order_code',0);
        $orderVnpay = $request->get('vnp_TxnRef',0);
        $code = $orderCode > 0 ? $orderCode : $orderVnpay;
        
        $data = $this->order->where('code',$code)->with('orderProducts','user')->first();
        if(!$data) {
            return redirect()->route('info','tab=order');
        }
        if($data->payment_type == 1 && $data->status == 0) {
            $this->order->where('id',$data->id)->update(['status'=>1]);
        }
        $data->status_des = config('constants.order_status')[$data->status];
        return view('user.success',compact('data'));
    }

    public function confirmPayment(Request $request) {
        $this->validate($request, [
            'payment_type' => 'required|numeric|min:0|max:1',
            'address' => 'required|max:100',
            'phone' => 'required|min:8|max:15',
            'total' => 'required|numeric',
            'total_pay' => 'required|numeric',
            'ship' => 'numeric',
            'sale' => 'numeric',
            'voucher_id' => 'numeric',
            'voucher_discount' => 'numeric'
        ]);
        
        $input = $request->only(
            'address', 'phone', 'total', 'products', 'voucher_discount',
            'total_pay', 'ship', 'sale', 'payment_type', 'voucher_id'
        );
        Session::forget('payment');
        Session::put('payment', $input);
        
        try {
            if($input['payment_type'] == 1) {
                $this->vnPay($request);
                $order = $this->saveOrder($input, true);
            } else {
                if(!$request) {
                    return json_encode([
                        'status' => 401,
                        'message' => __('Fail'),
                    ]);
                } else {
                    $order = $this->saveOrder($input);
                    return json_encode([
                        'status' => 200,
                        'message' => __('Success'),
                        'data' => $order
                    ]);
                }
            }
        } catch (\Throwable $th) {
            return json_encode([
                'status' => 400,
                'message' => __('Fail'),
            ]);
        }
        
    }
    
    public function saveOrder($input, $vnpay = false) {
        // Lưu thông địa chỉ và sđt
        $user = Auth::user();
        if(!$user->address || !$user->phone) {
            $user->address = $input['address'];
            $user->phone = $input['phone'];
            $user->save();
        }
        $input['user_id'] = $user->id;
        // Lưu đơn hàng thanh toán code
        $input['code'] = 'OR'.time();
        $input['status'] = 0;
        $order = $this->order->create($input);

        if($input['voucher_id'] > 0) {
            $voucherUser = new VoucherUser();
            $voucherUser->user_id = $user->id;
            $voucherUser->voucher_id = $input['voucher_id'];
            $voucherUser->save();
        }
        // Lưu sản phẩm
        $orderProducts = [];
        $cartIds = [];
        $carts = Session::get('checkout');
        foreach($carts as $cart) {
            $item = [
                'order_id' => $order->id,
                'product_id' => $cart->products->id,
                'quantity' => $cart->quantity,
                'product_name' => $cart->products->name,
                'thumbnail' => $cart->products->thumbnail,
                'price' => $cart->products->price,
                'pay_price' => $cart->products->sale > 0 ? $cart->products->price * (100 - $cart->products->sale) /100 : $cart->products->price
            ];
            array_push($orderProducts,$item);
            if($cart->id) {
                array_push($cartIds,$cart->id);
            }

            $product = Products::find($cart->products->id);
            $product->increment('buycnt',$cart->quantity);
            $product->decrement('quantity',$cart->quantity);
        }
        OrderProducts::insert($orderProducts);
        Cart::whereIn('id',$cartIds)->delete();
        return $order;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }
}
