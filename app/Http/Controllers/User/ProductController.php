<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Products;

class ProductController extends Controller
{
    private $product;
    function __construct(
        Products $product
    ) {
        $this->product = $product;
    }

    public function index(Request $request){
        return view('user.product');
    }

    public function getList(Request $request) {
        $this->validate($request,[
            'category' => 'numeric',
            'price' => 'numeric'
        ]);

        $query = Products::where('status',1);
        $category = $request->get('category',0);
        $keyword = $request->get('keyword','');

        if($keyword) 
            $query->where('name', 'like','%'.$keyword.'%');

        if($category > 0 ) {
            $category = $request->get('category',0);
            $cats = Category::select('id')->where('parent',$category)->get()->toArray();
            $query = $query->whereIn('category_id',$cats)->orWhere('category_id', $category);
        } 

        $price = $request->get('price',0);
        if($price > 0) {
            switch ($price) {
                case 1:
                    $query = $query->where('price','<=', 2000000);
                    break;
                case 2:
                    $query = $query->whereBetween('price',[2000000,4000000]);
                    break;
                case 3:
                    $query = $query->whereBetween('price',[4000000,7000000]);
                    break;
                case 4:
                    $query = $query->whereBetween('price',[7000000,13000000]);
                    break;
                case 5:
                    $query = $query->where('price','>=',13000000);
                    break;
                default:
                    $query = $query;
                    break;
            }
        }
        $product = $query->where('status',1)->paginate(10);
        return json_encode([
            'status' => 200,
            'data' => $product,
            'message' => __('Success'),
        ]);
    }

    public function category() {
        $category = Category::where('parent',0)
            ->where('hide',1)
            ->with([
                'child' => function($q) {
                    $q->withCount('getProducts');
                }
            ])
            ->withCount('getProducts')->get();

        return json_encode([
            'status' => 200,
            'data' => $category,
            'message' => __('Success'),
        ]);
    }
}
