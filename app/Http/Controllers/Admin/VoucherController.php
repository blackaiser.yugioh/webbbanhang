<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Voucher;

class VoucherController extends Controller
{
    private $voucher;
    public function __construct(Voucher $voucher) {
        $this->voucher = $voucher;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type',0);
        $keyword = $request->get('k');

        $query = $this->voucher->query();

        /**
         * Tìm kiếm
         */
        if($type == 1) {
            $query->where('name','LIKE', '%'.$keyword.'%');
        } else if($type == 2) {
            $query->where('code','LIKE', '%'.$keyword.'%');
        }
        $vouchers = $query->orderby('id',"DESC")->paginate(10);
        return view('admin.voucher.index',compact('vouchers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.voucher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'discount' => 'required|numeric', 
            'name' => 'required|max:50', 
            'description' => 'required|max:100',
            'min' => 'required',
        ]);
        
        $input = $request->only([
            'discount', 'name', 'description', 'min'
        ]);

        $end = strtotime($request->get('end')) ?? 0;
        $start = strtotime($request->get('start')) ?? 0;
        
        if($end > 0 && $start > 0){
            $input['start'] = $start;
            $input['end'] = $end;
        }

        $input['code'] = 'VO' . time();
        $this->voucher->create($input);
        return redirect()->route('voucher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $voucher = $this->voucher->find($id);
        return view('admin.voucher.create', compact('voucher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'discount' => 'required|numeric', 
            'name' => 'required|max:50', 
            'description' => 'required|max:100',
            'min' => 'required',
        ]);
        $input = $request->only([
            'discount', 'name', 'description', 'min'
        ]);
        $end = strtotime($request->get('end')) ?? 0;
        $start = strtotime($request->get('start')) ?? 0;
        
        if($end > 0 && $start > 0){
            $input['start'] = $start;
            $input['end'] = $end;
        }
        $this->voucher->find($id)->update($input);
        return redirect()->route('voucher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hide($id){
        $voucher = $this->voucher->find($id);
        if ($voucher->status === 1) {
            $voucher->status = 0;
        } else {
            $voucher->status = 1;
        }
        $voucher->save();
        return redirect()->back();
    }
}
