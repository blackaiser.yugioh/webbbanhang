<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    private $category;
    public function __construct(Category $category) {
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type',0);
        $keyword = $request->get('k');
        $query = $this->category->with([
            'getParent' => function($q) {
                return $q->where('hide',1);
            }
        ]);

        /**
         * Tìm kiếm
         */
        if($type == 1) {
            $query->where('name','LIKE', '%'.$keyword.'%');
        }
        $cats = $query->orderby('id',"DESC")->paginate(10);

        return view('admin.category.index', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cat = new Category();
        $cats = $this->category->where('hide',1)->where('parent',0)->get();

        return view('admin.category.create', compact('cats','cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:50|unique:category',
            'code' => 'required|max:20|unique:category',
            'thumbnail' => 'required|image',
            'parent' => 'numeric',
        ],[
            'name.required' => 'Tên danh mục không được bỏ trống',
            'name.unique' => 'Tên danh mục đã tồn tại',
            'code.required' => 'Mã danh mục không được bỏ trống',
            'code.unique' => 'Mã danh mục đã tồn tại',
            'thumbnail.required' => 'Ảnh đại diện không được bỏ trống',
            'name.max' => 'Tên danh mục không được lớn hơn 50 ký tự',
            'code.max' => 'Mã danh mục không được lớn hơn 20 ký tự',
            'thumbnail.image' => 'Ảnh đại diện không đúng định dạng'
        ]);
        $input = $request->only([
            'name', 'code', 'parent'
        ]);

        $request->get('hide', null) ? $input['hide'] = 1 : $input['hide'] = 0;

        if ($request->file('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameHash = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $filePath = $request->file('thumbnail')->storeAs('public/category', $fileNameHash);
            $input['thumbnail'] = Storage::url($filePath);
        }
        $this->category->insert($input);

        return redirect()->route('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = $this->category->find($id);
        $cats = $this->category->where('hide',1)->where('parent',0)->get();

        return view('admin.category.create', compact('cats','cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|max:50|unique:category,name,'.$id,
            'code' => 'required|max:20|unique:category,code,'.$id,
            'thumbnail' => 'image',
            'parent' => 'numeric',
        ],[
            'name.required' => 'Tên danh mục không được bỏ trống',
            'name.unique' => 'Tên danh mục đã tồn tại',
            'code.required' => 'Mã danh mục không được bỏ trống',
            'code.unique' => 'Mã danh mục đã tồn tại',
            'name.max' => 'Tên danh mục không được lớn hơn 50 ký tự',
            'code.max' => 'Mã danh mục không được lớn hơn 20 ký tự',
            'thumbnail.image' => 'Ảnh đại diện không đúng định dạng'
        ]);
        $input = $request->only([
            'name', 'code', 'parent'
        ]);

        $request->get('hide', null) ? $input['hide'] = 1 : $input['hide'] = 0;

        if ($request->file('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameHash = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $filePath = $request->file('thumbnail')->storeAs('public/category', $fileNameHash);
            $input['thumbnail'] = Storage::url($filePath);
        }
        $this->category->find($id)->update($input);

        return redirect()->route('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hide($id){
        $cat = $this->category->find($id);
        if ($cat->hide === 1) {
            $cat->hide = 0;
        } else {
            $cat->hide = 1;
        }
        $cat->save();

        return redirect()->back();
    }
}
