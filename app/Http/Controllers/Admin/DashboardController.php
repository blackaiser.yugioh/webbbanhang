<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\User;
use App\Models\Category;
use App\Models\Products;
use App\Models\OrderProducts;
use Illuminate\Support\Carbon;
use DB; 


class DashboardController extends Controller
{
    public function index(Request $request) {
        $this->validate($request, [
            'start' => 'date',
            'end' => 'date',
        ]);

        /**
         * Top sản phẩm bán chạy theo tháng
         */
        $time = $request->get('time');
        if(!$time) {
            $request->merge(['time' => Carbon::now()->format('Y-m')]);
        }
        $date = new Carbon( $time ); 
        $orderid = Orders::select('created_at', 'id')
            ->whereMonth('created_at', $date->month)
            ->whereYear('created_at',$date->year)
            ->whereIn('status',[0,1,2,3])
            ->get()
            ->pluck('id');
        $products = OrderProducts::whereIn('order_id', $orderid)
            ->with([
                'product' => function($query){
                    // return $query->select('quantity');
                }
            ])
            ->get()
            ->groupBy('product_name')
            ->each(function($q){
                $q->count = count($q);
                $q->total_quantity = $q->sum('quantity');
            });
        $products = collect($products)->sortByDesc('total_quantity')->all();
        /**
         * thống kê doanh thu
         */
        $startDate = Carbon::parse($request->get('start'));      
        $endDate = Carbon::parse($request->get('end')); 

        $duration = $startDate->diff($endDate);
        
        if($duration->days === 0 || $duration->days > 30) {
            $startDate = Carbon::now()->startOfWeek(Carbon::SUNDAY);        
            $endDate = Carbon::now()->endOfWeek(Carbon::SATURDAY);

            $request->merge(['start' => $startDate->format('Y-m-d')]);
            $request->merge(['end' => $endDate->format('Y-m-d')]);
        }
        
        $dateInterval = \DateInterval::createFromDateString('1 day');
        $datePeriod = new \DatePeriod($startDate, $dateInterval, $endDate->modify('+1 day'));

        $orders = Orders::whereBetween('created_at',[$startDate,$endDate])
            ->with('orderProducts')
            ->get()
            ->groupBy(function($date) {
                return Carbon::parse($date->created_at)->format('d/m/Y');
            })
            ->each(function($q){
                $q->count = count($q);
            });

        /**
         * đơn hàng
         */
        $orderToday = Orders::whereIn('status',[0,1,2,3])->whereDate('created_at', Carbon::today())->get();
        $totalOorderToday = count($orderToday);
        $totalMoneyToday = $orderToday->sum('total_pay');
        $totalMoneyYesterday = Orders::whereIn('status',[0,1,2,3])->whereDate('created_at', Carbon::yesterday())->sum('total_pay');
        $compare = $totalMoneyYesterday ? number_format(($totalMoneyToday - $totalMoneyYesterday) / $totalMoneyYesterday, 4) : $totalMoneyToday;

        /**
         * người dùng
         */
        $userToday = User::whereDate('created_at', Carbon::today())->count();
        $userYesterday = User::whereDate('created_at', Carbon::yesterday())->count();
        $compareUser = $userYesterday > 0 ? number_format(($userToday - $userYesterday) / $userYesterday, 4) : $userToday;

        $payOnline = $orderToday->where('payment_type', 1)->sum('total_pay');
        return view('admin.dashboards', compact(
            'totalOorderToday', 'totalMoneyToday','compare', 'totalMoneyYesterday', 'payOnline', 
            'userToday', 'compareUser' ,'datePeriod','orders', 'products'
        ));
    }

    public function detail($date) {
        $date = Carbon::createFromTimestamp($date);
        $orders = Orders::select('created_at', 'id')
            ->whereDate('created_at', $date)
            ->whereIn('status',[0,1,2,3])
            ->get()->pluck('id');
        $products = OrderProducts::whereIn('order_id', $orders)->get()->groupBy('product_name')
        ->each(function($q){
            $q->count = count($q);
            $q->total_quantity = $q->sum('quantity');
        });

        return view('admin.detail', compact(
            'products', 'date'
        ));
    }
}
