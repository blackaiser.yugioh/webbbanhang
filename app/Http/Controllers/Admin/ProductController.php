<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\ProductImages;
use Illuminate\Http\Request;
use App\Models\Products;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    private $product;
    private $category;
    public function __construct(Products $product, Category $category) {
        $this->product = $product;
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type',0);
        $keyword = $request->get('k');

        $query = $this->product->with('category');

        /**
         * Tìm kiếm
         */
        if($type == 1) {
            $query->where('name','LIKE', '%'.$keyword.'%');
        }
        $products = $query->orderby('id',"DESC")->paginate(10);

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = $this->category->where('hide',1)->get();
        $product = new Products();
        return view('admin.products.create', compact('cats','product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "name" => 'required|max:50|unique:products',
            "price" => 'required|numeric',
            "quantity" => 'required|numeric',
            "category_id" => 'required|numeric',
            "short" => 'required',
            "description" => 'required',
            "thumbnail" => 'required|image',
            "images" => 'required|array',
            "images.*" => 'image',
            "sale" => 'numeric|digits_between:1,99'
        ]);
        

        $input = $request->only([
            "name", "price", "quantity", "category_id", "short", "description","sale"
        ]);
        $input['slug'] = Str::slug($input['name'],'-');
        $request->get('status', null) ? $input['status'] = 1 : $input['status'] = 0;
        if ($request->file('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameHash = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $filePath = $request->file('thumbnail')->storeAs('public/products', $fileNameHash);
            $input['thumbnail'] = Storage::url($filePath);
        }
        $product = $this->product->create($input);
        $images = $request->images;
        if(count($images) > 0) {
            $dataImg = [];
            foreach($images as $image) {
                $fileNameHash = Str::random(20) . '.' . $image->getClientOriginalExtension();
                $filePath = $image->storeAs('public/products', $fileNameHash);
                $data['product_id'] = $product->id;
                $data['url'] = Storage::url($filePath);
                array_push($dataImg, $data);   
            }
            ProductImages::insert($dataImg);
        }
        return redirect()->route('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = $this->product->where('id', $id)->with('images')->firstOrFail();
        $cats = $this->category->where('hide',1)->get();
        return view('admin.products.create', compact('product','cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "name" => 'required|max:50|unique:products,name,'.$id,
            "price" => 'required|numeric',
            "quantity" => 'required|numeric',
            "category_id" => 'required|numeric',
            "short" => 'required',
            "description" => 'required',
            "thumbnail" => 'image',
            "images" => 'array',
            "images.*" => 'image',
            "sale" => 'numeric|digits_between:1,99'
        ]);
        

        $input = $request->only([
            "name", "price", "quantity", "category_id", "short", "description", "sale"
        ]);
        $input['slug'] = Str::slug($input['name'],'-');
        $request->get('status', null) ? $input['status'] = 1 : $input['status'] = 0;
        if ($request->file('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameHash = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $filePath = $request->file('thumbnail')->storeAs('public/products', $fileNameHash);
            $input['thumbnail'] = Storage::url($filePath);
        }
        $this->product->find($id)->update($input);
        $images = $request->images;
        if(!empty($images) && count($images) > 0) {
            $dataImg = [];
            foreach($images as $image) {
                $fileNameHash = Str::random(20) . '.' . $image->getClientOriginalExtension();
                $filePath = $image->storeAs('public/products', $fileNameHash);
                $data['product_id'] = $id;
                $data['url'] = Storage::url($filePath);
                array_push($dataImg, $data);   
            }
            ProductImages::insert($dataImg);
        }
        return redirect()->route('product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hide($id){
        $product = $this->product->find($id);
        if ($product->status === 1) {
            $product->status = 0;
        } else {
            $product->status = 1;
        }
        $product->save();
        return redirect()->back();
    }

    public function image($id)
    {
        try {
            ProductImages::findOrFail($id)->delete();
            return response()->json([
                'code' => 200,
                'message' => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 404,
                'message' => $th->getMessage(),
            ], 404);
        }
    }
}
