<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Jobs\SendOTP as JobOTP;

use function PHPSTORM_META\type;

class UserController extends Controller
{
    private $user;
    public function __construct(User $user) {
        $this->user = $user;
    }
    public function login(Request $request){
        if (Auth::check()) {
            // nếu đăng nhập thàng công thì 
            return redirect('/');
        } else {
            return view('auth.login');

        }
    }
    public function signIn(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ],
        [
            'email.required' => 'Email không được bỏ trống.',
            'password.required' => 'Mật khẩu không được bỏ trống.',
        ]);

        $login = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($login)) {
            if(Auth::user()->lock === 0) {
                Auth::logout();
                
                return redirect()->back()->with('status', 'Tài khoản của bạn đã bị khóa. Vui lòng liên hệ admin để được hỗ trợ.');
            }
            return redirect('/');
        } else {
            return redirect()->back()->with('status', 'Email và mật khẩu không chính xác');
        }
    }

    public function register() {
        return view('auth.register');
    }

    public function postRegister(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:50|min:5',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8|regex:/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]).{8,}$/',
        ],
        [
            'name.required' => 'Tên người dùng không được bỏ trống.',
            'name.max' => 'Tên người dùng không được quá 50 ký tự.',
            'name.min' => 'Tên người dùng phải lơn hơn 5 ký tự.',
            'email.required' => 'Email không được bỏ trống.',
            'email.unique' => 'Email đã tồn tại.',
            'password.required' => 'Mật khẩu không được bỏ trống.',
            'password.min' => 'Mật khẩu tổi thiểu 8 ký tự.',
            'password.regex' => 'Mật khẩu phải chứa chữ in hoa, chữ thường, số và kí tự',
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        
        $login = [
            'email' => $user->email,
            'password' => $request->password,
        ];
        Auth::attempt($login);
        
        return redirect('/');
    }

    public function signOut(){
        Auth::logout();
        return view('auth.login');
    }

    function index(Request $request) {
        $type = $request->get('type',0);
        $keyword = $request->get('k');
        
        $query =  User::whereDoesntHave('roles', function ($q) {
            $q->where('name','super_admin');
        });

        /**
         * Tìm kiếm
         */
        if($type == 1) {
            $query->where('name','LIKE', '%'.$keyword.'%');
        } else if($type == 2) {
            $query->where('email', 'LIKE', '%'.$keyword.'%');
        }
        $users = $query->orderby('id',"DESC")->paginate(10);

        return view('admin.user.index',compact('users'));
    }

    function lock($id) {
        $user = $this->user->find($id);
        if ($user->lock === 1) {
            $user->lock = 0;
        } else {
            $user->lock = 1;
        }
        $user->save();
        return redirect()->back();
    }

    public function forgot() {
        return view('auth.forgot-password');
    }

    public function sendOTP(Request $request) {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email'
        ],[
            'email.required' => 'Vui lòng nhập email.',
            'email.email' => 'Không đúng định dạng.',
            'email.exists' => 'Email không tồn tại.',
        ]);
        $email = $request->get('email');
        $otp = mt_rand(100000, 999999);
        $encode = json_encode([
            'otp' => $otp,
            'expired' => time()+ 180
        ]);
        User::where('email', $email)->update(['otp' => $encode]);
        $this->dispatch(new JobOTP($otp, $email));
        return json_encode([
            'status' => 200,
            'message' => __('Success'),
        ]);
    }

    public function confirmOtp(Request $request) {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'otp' => 'required|numeric|digits:6'
        ],[
            'email.required' => 'Vui lòng nhập email.',
            'email.email' => 'Không đúng định dạng.',
            'email.exists' => 'Email không tồn tại.',
            'otp.required' => 'Vui lòng nhập otp.',
            'otp.numeric' => 'Không đúng định dạng số.',
            'otp.digits' => 'Không đúng định dạng.',
        ]);
        $email = $request->get('email');
        $otp = (int)$request->get('otp');
        $valueOtp = User::where('email',$email)->first();
        $decode = json_decode($valueOtp->otp);

        if(time() > $decode->expired) {
            User::where('email', $email)->update(['otp' => null]);
            return json_encode([
                'status' => 1001,
                'message' => __('OTP đã hết hạn sử dụng'),
            ]);
        } else if($otp != (int)$decode->otp) {
            return json_encode([
                'status' => 1002,
                'message' => __('OTP không chính xác!'),
            ]);
        }

        $text = Str::random(15);
        User::where('email', $email)->update(['otp' => $text]);
        return json_encode([
            'status' => 200,
            'data' => ['key' => $text],
            'message' => __('Success'),
        ]);
    }

    public function updatePass(Request $request){
        $this->validate($request, [
            'key' => 'required|min:15|exists:users,otp',
            'password' => 'required|string|min:8|regex:/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]).{8,}$/'
        ],[
            'key.required' => 'Không đúng định dạng.',
            'key.min' => 'Không đúng định dạng.',
            'password.required' => 'Không được để trống mật khẩu.',
            'password.min' => 'Mật khẩu tối thiểu 8 ký tự.',
            'password.regex' => 'Mật khẩu phải chứa chữ in hoa, chữ thường, số và kí tự',
        ]);

        $user = User::where('otp', $request->get('key'))->first();
        $user->update([
            'password' => Hash::make($request->get('password')),
            'otp' => null
        ]);

        return json_encode([
            'status' => 200,
            'message' => __('Success'),
        ]);
    }
}
