<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::orderby('id',"DESC")->paginate(20);

        return view('admin.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = new Posts();
        return view('admin.post.create', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'required|image',
            'description' => 'required',
        ]);

        $input = $request->only('title', 'description');

        $input['user_id'] = Auth::id();
        $input['status'] = 1;

        if ($request->file('image')) {
            $file = $request->file('image');
            $fileNameHash = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $filePath = $request->file('image')->storeAs('public/products', $fileNameHash);
            $input['image'] = Storage::url($filePath);
        }
        $input['slug'] = Str::slug($input['title'],'-');

        Posts::create($input);
        return redirect()->route('post');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Posts::find($id);
        return view('admin.post.create', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'image' => 'image',
            'description' => 'required',
        ]);

        $input = $request->only('title', 'description');
        
        if($request->hasFile('image')) {
            $file = $request->file('image');
            $fileNameHash = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $filePath = $request->file('image')->storeAs('public/products', $fileNameHash);
            $input['image'] = Storage::url($filePath);
        }

        Posts::find($id)->update($input);
        return redirect()->route('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hide($id) {
        $post = Posts::find($id);
        $post->update([
            'status' => $post->status == 1 ? 0 : 1
        ]);
        return redirect()->route('post');
    }
}
