<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    private $review;
    public function __construct(Review $review) {
        $this->review = $review;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = $this->review->with([
            'user' => function($q){
                $q->select('name','id');
            },
            'product' => function($qq){
                $qq->select('name','id');
            }
        ])->orderby('id',"DESC")->paginate(20);
        return view('admin.products.review', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $review = $this->review->find($id);

        if($review->status == 0) {
            $product = Products::find($review->product_id);
            $listReview = Review::select('star')->where('product_id', $review->product_id)->where('status',1)->get();

            $total = count($listReview);
            $totalPoint = $listReview->sum('star');

            $product->update([
                'point'=>number_format($totalPoint/$total,1),
                'review' => $product->review + 1
            ]);
        }
        
        $review->status === 0 ? $review->status = 1 : $review->status = 0;

        $review->save();
        return redirect()->back();        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
