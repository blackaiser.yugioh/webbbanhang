<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $order;
    function __construct(
        Orders $order
    ) {
        $this->order = $order;
    }
    public function index(Request $request)
    {
        $phone = $request->get('phone');
        $code = $request->get('code','');
        $date = $request->get('date','');
        $status = $request->get('status',10);
        $name = $request->get('name','');
        $query = $this->order->with([
            'user' => function($q) {
                $q->select('id','name');
            }
        ]);

        /**
         * Tìm kiếm
         */
        if($code)
        $query->where('code','LIKE', '%'.$code.'%');

        if($date) 
        $query->whereDate('created_at',$date);

        if($phone) 
        $query->where('phone','like', '%'.$phone.'%');

        if($status < 10)
        $query->where('status', $status);

        if($name) {
            $userId = User::select('id','name')->where('name','like', '%'. $name .'%')->pluck('id');
            $query->whereIn('user_id', $userId);
        }

        $orders = $query->orderBy('id','DESC')->paginate(10);
        // dd($orders);
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->order->where('id',$id)->with([
            'orderProducts', 
            'user' => function($q){
                $q->select('name','id');
            }
        ])->first();
        $order->status_des = config('constants.order_status')[$order->status];

        $s = config('constants.order_status');
        $status = [];
        foreach($s as $k => $v) {
            if($k >= $order->status && $k < 6) {
                $status[] = [
                    'key' => $k,
                    'text' => $v
                ];
            }
        }

        // dd($status);
        return view('admin.orders.detail', compact('order', 'status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required'
        ]);
        $order = Orders::find($request->get('id'));
        $order->status = $request->get('status');
        $order->save();
        return json_encode([
            'status' => 200,
            'data' => $order,
            'message' => __('Success'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
