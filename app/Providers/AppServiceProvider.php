<?php

namespace App\Providers;

use App\Models\Cart;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
  private $user;

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    $user = Auth::check();
    $userid =  Auth::user();
    Paginator::useBootstrap();

    //format blade

    Blade::directive('money_vn', function ($number) {
      return "<?php echo GeneralFormat::price($number); ?>";
    });
    
    View::composer('*', function($view){
      if (Auth::check()){
        $cart = Cart::where('user_id', Auth::id())->count();
        $view->with('countCart', $cart);
      }
    });

    $count = 0;
    DB::listen(function ($query) use (&$count) {
        Log::info(
            $query->sql,
            $query->bindings,
            $query->time
        );
        $count++;
        Log::info("number query $count");
    });
  }
}
