<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class MailOTP extends Mailable
{
    use Queueable, SerializesModels;
    private $otp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($otp)
    {
        $this->otp = $otp;
    }

    public function build()
    {
        $otp = $this->otp;
        return $this->view('user.mail',compact('otp'))->subject(__('Mã xác thực tài khoản'));
    }
}
