<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(30)->create();
        Role::create(['name' => 'super_admin']);
        $user = new User();
        $user->name = 'admin';
        $user->password = Hash::make('123456@');
        $user->email = 'admin@gmail.com';
        $user->save();
        $user->syncRoles(['super_admin']);
    }
}
