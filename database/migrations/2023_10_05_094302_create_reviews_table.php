<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('product_id');
            $table->tinyInteger('type')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0: ẩn, 1 hiển thị');
            $table->string('name',50)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('email',50)->nullable();
            $table->text('comment')->nullable();
            $table->integer('star')->default(5);
            $table->tinyInteger('reply');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
};
