<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->index();
            $table->string('address',150);
            $table->string('phone',15);
            $table->float('total',13,2);
            $table->float('total_pay',13,2);
            $table->float('ship',10,2)->default(0);
            $table->float('sale')->default(0);
            $table->string('payment_type')->comment('0: cod, 1: online');
            $table->integer('status')->default(1);
            $table->timestamps();
        });

        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id')->index();
            $table->integer('product_id')->index();
            $table->integer('quantity');
            $table->string('product_name');
            $table->string('thumbnail');
            $table->float('price',13,2);
            $table->float('pay_price',13,2);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_products');
    }
};
