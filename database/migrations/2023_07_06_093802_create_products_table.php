<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->index();
            $table->string('name',100);
            $table->string('thumbnail',100);
            $table->boolean('status')->default(1)->comment('0: ẩn, 1 hiển thị');
            $table->integer('quantity')->default(0);
            $table->integer('buycnt')->default(0);
            $table->float('price',10,2)->default(0);
            $table->integer('sale')->default(0);
            $table->text('short')->nullable();
            $table->text('description')->nullable();
            $table->float('point',2,1)->default(5);
            $table->tinyInteger('review')->default(5);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
