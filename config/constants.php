<?php
return [
    'order_status'=>[
        0 => 'Chờ thanh toán',
        1 => 'Đã thanh toán',
        2 => 'Đang giao hàng',
        3 => 'Đã hoàn thành',
        4 => 'Đã hủy',
        5 => 'Trả hàng/hoàn tiền',
        6 => 'Thanh toán không thành công'
    ],
];
