import axios from 'axios'

const instance = axios.create({
  baseURL:window.location.origin,
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
  },
});


export const http = instance;
