
require('./bootstrap');

import { createApp } from 'vue';
import DetailProductComponent from './components/DetailProductComponent';
import CartComponent from './components/CartComponent.vue';
import CheckoutComponent from './components/CheckoutComponent.vue';
import SuccessComponent from './components/SuccessComponent.vue';
import InfoComponent from './components/InfoComponent.vue';
import ProductComponent from './components/ProductComponent.vue';
import ListOrderComponent from './components/child/ListOrderComponent.vue';
import ModalUpdaterOrderComponent from './components/child/ModalUpdaterOrderComponent.vue';
import AddressComponent from './components/child/AddressComponent.vue';
import ComtactComponent from './components/ContactComponent.vue';
import ForgotPasswordComponent from './components/child/ForgotPasswordComponent.vue';
import store from './stores';

const app = createApp({});
app.use(store);
app.component('detail-product', DetailProductComponent);
app.component('contact', ComtactComponent);
app.component('info-address', AddressComponent);
app.component('list-order', ListOrderComponent);
app.component('cart', CartComponent);
app.component('checkout', CheckoutComponent);
app.component('success', SuccessComponent);
app.component('info', InfoComponent);
app.component('modal-update-status', ModalUpdaterOrderComponent);
app.component('product-list', ProductComponent);
app.component('forgot-password',ForgotPasswordComponent)
app.mount('#app');