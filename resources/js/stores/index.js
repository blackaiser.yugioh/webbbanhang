
import Vue from 'vue';
import { createStore } from 'vuex'

import { auth } from "./auth";
const store = createStore({
    modules: {
        auth
    },
    state: {
        globalData: {
            setting: {}
        },
    },
    getters: {
    },
    mutations: {
        setGlobalData (state, data) {
            state.globalData = data;
        }
    },
    actions: {}
})

export default store;