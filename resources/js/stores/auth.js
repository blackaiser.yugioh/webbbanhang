import { http } from './../config/http';

export const auth = {
    actions: {
        review({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('review', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        getReview({ commit }, id) {
            return new Promise((resolve, reject) => {
                http.get('list-review/' + id).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        addToCart({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('addToCart', data).then(response => {
                    let resData = response.data;
                    console.log(resData);
                    if(resData.status === 401) {
                        window.location.href = '/sign-in'
                    }
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        deleteCart({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('delete-cart', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        postOrder({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('post-checkout', data).then(response => {
                    let resData = response.data;
                    if(resData.status === 200){
                        window.location.href = '/checkout'
                    }
                    
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        listCity({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.get('list-city').then(response => {
                    let resData = response.data;
                    
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        listDistrict({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('list-district', data).then(response => {
                    let resData = response.data;
                    resolve(resData);
                }).catch(err => {
                    reject(err);
                })
            })
        },
        listArea({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('list-area', data).then(response => {
                    let resData = response.data;
                    resolve(resData);
                }).catch(err => {
                    reject(err);
                })
            })
        },
        payment({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('payment', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        listOrder({ commit }, status) {
            return new Promise((resolve, reject) => {
                http.get('don-hang?status=' + status).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        updateUser({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('update-user', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        cancelOrder({ commit }, id) {
            return new Promise((resolve, reject) => {
                http.get('cancel-order/'+ id).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        updateOrder({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('admin/order/update', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        listProduct({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('product-list',data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        listCategory({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.get('category-list').then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        sendContact({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('gui-lien-he', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        sendOTP({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('sendOTP', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        confirmOtp({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('confirmOtp', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        updatePass({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('updatePass', data).then(response => {
                    let resData = response.data;
                    resolve(resData);

                }).catch(err => {
                    reject(err);
                })
            })
        },
        changePsss({ commit }, data) {
            return new Promise((resolve, reject) => {
                http.post('change-password', data).then(response => {
                    let resData = response.data;
                    resolve(resData);
                }).catch(err => {
                    reject(err);
                })
            })
        },
    },
}
