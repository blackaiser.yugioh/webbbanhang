<header id="header" class="header align-items-center" >
  <div class="main">
      <i class="mobile-nav-toggle mobile-nav-show bi bi-list" style="color: black; font-size: 33px; padding-left: 12px;"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
      <div class="header-main container">
          <div class="row">
              <div class="col-xl-4 col-lg-4 col-12">
                  <a href="/" class="d-flex align-items-center justify-content-center">
                      <img height="60" src="{{ asset('assets/img/favicon/logo.png') }}" alt="logo" />
                      <h3 class="my-auto">Khánh Store</h3>
                  </a>
              </div>
              <div class="col-xl-4 col-lg-4 col-12 align-items-center form-search pb-1">
                  <div class="search w-100">
                      <form method="GET" action="{{ route('home.list-product') }}" class="search-bar d-flex">
                          <div class="box w-100">
                              <div class="container-3">
                                  <span class="icon"><i class="bx bx-search"></i></span>
                                  <input value="{{request('keyword')}}" type="search" id="search" name="keyword" placeholder="Tìm kiếm..." />
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-12 d-flex justify-content-center">
                <div class="d-grid px-3">
                  <a href="{{route('home.cart')}}" type="button" class="btn pb-0">
                    <i class="bx bx-cart fs-30"></i>
                    <span class='badge badge-warning' id='lblCartCount'> {{ $countCart ?? ''}} </span>
                  </a>
                  <span class="text-center">Giỏ hàng</span>
                </div>
                <div class="d-grid px-3">
                  <button type="button" class="btn pb-0">
                    <i class="bx bx-user fs-30"></i>
                  </button>
                  @if(auth()->check())
                    <span class="nav-item">
                      <a class="nav-link text-center" href="#" data-bs-toggle="dropdown">{{ auth()->user()->name }}</a>
                      <ul class="dropdown-menu">
                        @if(auth()->user()->hasRole('super_admin'))
                          <li><a class="dropdown-item" href="{{ route('dashboard') }}">Trang quản trị</a></li>
                        @endif
                        <li><a class="dropdown-item" href="{{route('info','tab=profile')}}">Tải khoản của tôi</a></li>
                        <li><a class="dropdown-item" href="{{route('info','tab=order')}}">Đơn mua</a></li>
                        <li><a class="dropdown-item" href="{{ route('sign-out') }}">Đăng xuất</a></li>
                      </ul>
                    </span>
                  @else
                    <a href="{{ route('login') }}" class="text-center">Đăng nhập</a>
                  @endif
                  
                </div>
              </div>
          </div>
      </div>
  </div>
  <div class="w-100 d-flex align-items-center justify-content-center" style="background-color:black;">       
      <div class="container d-flex justify-content-center">
          <nav id="navbar" class="navbar" >
              <ul>
                  <li><a href="/">Trang chủ</a></li>
                  <li><a href="{{ route('home.about') }}">Giới thiệu</a></li>
                  <!-- <li><a href="" >Catalogue</a></li> -->
                  <li><a href="{{ route('home.list-product') }}">Sản phẩm</a></li>
                  <li><a href="{{ route('home.post') }}">Tin tức</a></li>
                  <li><a href="{{ route('home.contact') }}">Liên hệ</a></li>
              </ul>
          </nav>
      </div>
      <!-- .navbar -->
  </div>
</header>
<!-- End Header -->
