<footer class="{{ (Request::segment(1) == 'cart') ? 'd-none' : '' }} bg-white mt-4">
  <div class="container pt-5">
    <div class="row py-4">
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <img src="{{asset('assets/img/favicon/logo.png')}}" alt="" width="100" class="mb-3">
        <p class="font-italic text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
        <ul class="list-inline mt-4">
          <li class="list-inline-item"><a href="#" target="_blank" title="twitter"><i class="fa-brands fa-twitter"></i></a></li>
          <li class="list-inline-item"><a href="#" target="_blank" title="facebook"><i class="fa-brands fa-facebook"></i></a></li>
          <li class="list-inline-item"><a href="#" target="_blank" title="instagram"><i class="fa-brands fa-instagram"></i></a></li>
          <li class="list-inline-item"><a href="#" target="_blank" title="pinterest"><i class="fa-brands fa-pinterest"></i></a></li>
          <li class="list-inline-item"><a href="#" target="_blank" title="vimeo"><i class="fa-brands fa-vimeo"></i></a></li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <h4 class="fw-bold mb-4">Khánh Store</h6>
        <ul class="list-unstyled mb-0">
          <li class="mb-2"><a href="#" class="text-muted">Giới thiệu</a></li>
          <li class="mb-2"><a href="{{ route('home.list-product') }}" class="text-muted">Sản phẩm</a></li>
          <li class="mb-2"><a href="{{ route('home.cart') }}" class="text-muted">Giỏ hàng</a></li>
          <li class="mb-2"><a href="{{ route('info') }}" class="text-muted">Thông tin cá nhân</a></li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-6 mb-lg-0">
        <h4 class="fw-bold mb-4">Thông tin địa chỉ</h4>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d931.1731786426293!2d105.93167584388779!3d21.004951315699458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a94c1f882977%3A0x6d016e6656923f46!2zSOG7jWMgdmnhu4duIE7DtG5nIE5naGnhu4dwIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1696931746232!5m2!1svi!2s" 
          width="400" height="300" 
          style="border:0;" allowfullscreen="" 
          loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
      </div>
    </div>
  </div>

  <!-- Copyrights -->
  <div class="bg-light py-4">
    <div class="container text-center">
      <p class="text-muted mb-0 py-2">©2023, made with ❤️ by Khánh Store</p>
    </div>
  </div>
</footer>