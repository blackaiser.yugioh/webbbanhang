<!DOCTYPE html>

<html class="light-style layout-menu-fixed" data-theme="theme-default" data-assets-path="{{ asset('/assets') . '/' }}" data-base-url="{{url('/')}}" data-framework="laravel" data-template="vertical-menu-laravel-template-free">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

  <title>@yield('title') | Khánh Store </title>
  <meta name="description" content="{{ config('variables.templateDescription') ? config('variables.templateDescription') : '' }}" />
  <meta name="keywords" content="{{ config('variables.templateKeyword') ? config('variables.templateKeyword') : '' }}">
  <!-- laravel CRUD token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Canonical SEO -->
  <link rel="canonical" href="{{ config('variables.productPage') ? config('variables.productPage') : '' }}">
  <!-- Favicon -->
  <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon/logo.png') }}" />

  <!-- Include Styles -->
  @include('layouts/sections/styles')

  <!-- Include Scripts for customizer, helper, analytics, config -->
  @include('layouts/sections/scriptsIncludes')

  <!-- Ckeditor -->
  <script src="https://cdn.ckeditor.com/ckeditor5/38.1.0/classic/ckeditor.js"></script>

</head>

<body>
  @yield('layoutContent')
  <div id="app">
  </div>
  <!-- Layout Content -->
  <!--/ Layout Content -->

  <!-- Include Scripts -->
  @include('layouts/sections/scripts')
  @yield('page-script')
</body>
<script defer src="https://use.fontawesome.com/releases/v5.15.4/js/fontawesome.js" integrity="sha384-dPBGbj4Uoy1OOpM4+aRGfAOc0W37JkROT+3uynUgTHZCHZNMHfGXsmmvYTffZjYO" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/efd914f085.js" crossorigin="anonymous"></script>
<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
<script>
  function readURL(input) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();

        reader.onload = function (e) {
            $('#imgThumbnail').attr('src', e.target.result);
            $('#imgThumbnail').attr("class","d-block m-1");
        }
        reader.readAsDataURL(input.files[0]);
    }
  }
  $("#thumbnail").change(function(){
    readURL(this);

  });

  //multiple image
  let imagesPreview = function(input, placeToInsertImagePreview) {
    if (input.files) {
      let filesAmount = input.files.length;

        for (i = 0; i < filesAmount; i++) {
          let reader = new FileReader();

            reader.onload = function(event) {
                $($.parseHTML('<img>')).attr('src', event.target.result).attr('class','m-1').appendTo(placeToInsertImagePreview);
            }

            reader.readAsDataURL(input.files[i]);
        }
    }
  };

  $('#images').on('change', function() {
    imagesPreview(this, 'div.gallery');
  });

  ClassicEditor.create( document.querySelector( '#editor-1' ))
      .catch( error => {
        console.error( error );
      } );
    ClassicEditor.create( document.querySelector( '#editor' ))
      .catch( error => {
        console.error( error );
    } );
</script>
<script>
    const BASE_URL = "{{ url('/') }}";
</script>
</html>
