@extends('layouts/blankLayout')

@section('title', 'Đăng ký tài khoản')

@section('page-style')
<!-- Page -->
<link rel="stylesheet" href="{{asset('assets/vendor/css/pages/page-auth.css')}}">
@endsection


@section('content')
<div class="container-xxl">
  <div class="authentication-wrapper authentication-basic container-p-y">
    <div class="authentication-inner">

      <!-- Register Card -->
      <div class="card">
        <div class="card-body">
          <!-- Logo -->
          <div class="app-brand justify-content-center">
            <a href="{{url('/')}}" class="app-brand-link gap-2">
              <img height="60px" src="{{ asset('assets/img/favicon/logo.png') }}" alt="logo" />
            </a>
          </div>
          <!-- /Logo -->
          <h4 class="mb-2">Đăng ký tài khoản 🚀</h4>
          <p class="mb-4">Trải nghiệm các dịch vụ của chúng tôi!</p>
          @if (session('status'))
            <ul>
              <li class="text-danger"> {{ session('status') }}</li>
            </ul>
          @endif
          <form id="formAuthentication" class="mb-3" action="{{route('postRegister')}}" method="POST">
            {{ csrf_field() }}
            <div class="mb-3">
              <label for="name" class="form-label">Tên người dùng</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Vui lòng nhập tên người dùng">
              @if($errors->has('name'))
                <span class="text-danger"> {{ $errors->first('name') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label for="email" class="form-label">Email</label>
              <input type="text" class="form-control" id="email" name="email" placeholder="Vui lòng nhập email">
              @if($errors->has('email'))
                <span class="text-danger"> {{ $errors->first('email') }}</span>
              @endif
            </div>
            <div class="mb-3 form-password-toggle">
              <label class="form-label" for="password">Mật khẩu</label>
              <div class="input-group input-group-merge">
                <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
              </div>
              @if($errors->has('password'))
                <span class="text-danger"> {{ $errors->first('password') }}</span>
              @endif
            </div>

            <div class="mb-3">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="terms-conditions" name="terms" required>
                <label class="form-check-label" for="terms-conditions">
                  Tôi đồng ý với chính sách bảo mật và các điều khoản.
                </label>
              </div>
            </div>
            <button class="btn btn-primary d-grid w-100">
              Đăng ký
            </button>
          </form>

          <p class="text-center">
            <span>Bạn đã có tài khoản?</span>
            <a href="{{ route('login') }}">
              <span>Đăng nhập ngay</span>
            </a>
          </p>
        </div>
      </div>
    </div>
    <!-- Register Card -->
  </div>
</div>
</div>
@endsection
