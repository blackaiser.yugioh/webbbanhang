@extends('layouts/blankLayout')

@section('title', 'Đăng nhập')

@section('page-style')
<!-- Page -->
<link rel="stylesheet" href="{{asset('assets/vendor/css/pages/page-auth.css')}}">
@endsection

@section('content')
<div class="container-xxl">
  <div class="authentication-wrapper authentication-basic container-p-y">
    <div class="authentication-inner">
      <!-- Register -->
      <div class="card">
        <div class="card-body">
          <!-- Logo -->
          <div class="app-brand justify-content-center">
            <a href="{{url('/')}}" class="app-brand-link gap-2">
              <!-- <span class="app-brand-logo demo">@include('_partials.macros',["width"=>25,"withbg"=>'#696cff'])</span>
              <span class="app-brand-text demo text-body fw-bolder">{{config('variables.templateName')}}</span> -->
              <img height="60px" src="{{ asset('assets/img/favicon/logo.png') }}" alt="logo" />
            </a>
          </div>
          <!-- /Logo -->
          <h4 class="mb-2 text-center">Chào mừng bạn đến với <Strong>Khánh Store</Strong></h4>
          @if (session('status'))
            <span class="text-danger"> {{ session('status') }}</span>
          @endif
          <form id="formAuthentication" class="mb-3" action="{{ route('signIn') }}" method="POST">
            @csrf
            <div class="mb-3">
              <label for="email" class="form-label">Email</label>
              <input type="text" class="form-control" id="email" name="email" placeholder="Vui lòng nhập email" autofocus>
              @if($errors->has('email'))
                <span class="text-danger"> {{ $errors->first('email') }}</span>
              @endif
            </div>
            <div class="mb-3 form-password-toggle">
              <div class="d-flex justify-content-between">
                <label class="form-label" for="password">Mật khẩu</label>
                <a href="{{url('forgot-password')}}">
                  <small>Quên mật khẩu?</small>
                </a>
              </div>
              <div class="input-group input-group-merge">
                <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
              </div>
              @if($errors->has('password'))
                <span class="text-danger"> {{ $errors->first('password') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="remember-me">
                <label class="form-check-label" for="remember-me">
                  Ghi nhớ đăng nhập
                </label>
              </div>
            </div>
            <div class="mb-3">
              <button class="btn btn-primary d-grid w-100" type="submit">Đăng nhập</button>
            </div>
          </form>

          <p class="text-center">
            <span>Nếu bạn chưa có tài khoản</span>
            <a href="{{ route('sign-up')}}">
              <span>Đăng ký tại đây</span>
            </a>
          </p>
        </div>
      </div>
    </div>
    <!-- /Register -->
  </div>
</div>
</div>
@endsection
