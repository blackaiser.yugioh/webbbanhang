@extends('layouts/contentNavbarLayout')

@section('title', 'Quản lý voucher')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <h5 class="card-header">Tìm kiếm</h5>
      <div class="card-body demo-vertical-spacing demo-only-element col-7">
        <form method="GET" class="d-flex">
          <div class="input-group">
            <select class="form-select" name="type" style="max-width: 200px;">
              <option {{ request('type') == 1 ? "selected" : "" }} value="1">Tên voucher</option>
              <option {{ request('type') == 2 ? "selected" : "" }} value="2">Mã voucher</option>
            </select>
            <input value="{{request('k')}}" type="text" name="k" class="form-control" placeholder="Nhập từ khóa tìm kiếm....">
          </div>
          <button class="mx-1 btn btn-outline-primary" style="width: 150px;" type="submit">Tìm kiếm</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="card">
  <h5 class="card-header">Quản lý voucher</h5>
  <div class="card-body">
    <div class="table-responsive text-nowrap" style="padding-bottom: 100px;">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>STT</th>
            <th>Tên voucher</th>
            <th>Mã code</th>
            <th>Discount</th>
            <th>Giá trị đơn hàng tối thiểu</th>
            <th>Thời gian</th>
            <th>Trang thái</th>
          </tr>
        </thead>
        <tbody>
          @foreach($vouchers as $k => $voucher)
            <tr>
              <td>{{$k+1}}</td>
              <td>{{ $voucher->name }}</td>
              <td>{{ $voucher->code }}</td>
              <td>
                @money_vn($voucher->discount)
              </td>
              <td>
                @money_vn($voucher->min)
              </td>
              @if($voucher->start > 0 || $voucher->end > 0)
                <td>
                  {{ date('d/m/Y : H:i', $voucher->start) }}<br>{{ date('d/m/Y : H:i', $voucher->end) }} 
                </td>
                
              @else
                <td></td>
              @endif
              <td>
                <div class="d-flex w-100 justify-content-between">
                  @if(strtotime('today') > $voucher->end && $voucher->start >0 && $voucher->end > 0)
                    <span class="badge bg-label-warning me-1">Hết hạn</span>
                  @elseif($voucher->status === 1)
                    <span class="badge bg-label-success me-1">Hoạt động</span>
                  @else 
                    <span class="badge bg-label-secondary me-1">Bị ẩn</span>
                  @endif
                  <div class="dropdown">
                    <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{ route('voucher.edit', ['id' => $voucher->id]) }}"><i class="bx bx-edit-alt me-1"></i>Sửa</a>
                      <a class="dropdown-item" href="{{ route('voucher.hide', ['id' => $voucher->id]) }}"><i class="bx bx-hide me-1"></i>@if($voucher->status === 1) Ẩn @else Hiển thị @endif</a>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="d-flex mt-3 justify-content-center">
      {!! $vouchers->links() !!}
    </div>
  </div>
</div>
@endsection