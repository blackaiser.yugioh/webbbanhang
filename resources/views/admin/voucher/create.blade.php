@extends('layouts/contentNavbarLayout')

@section('title', 'Thêm voucher')
@section('content')

<div class="row">
    <div class="col-xl">
      <div class="card mb-4">
        <div class="card-header d-flex  align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{{ route('voucher') }}">Voucher</a>
              </li>
              <li class="breadcrumb-item active">
                <a href="{{ route('voucher.create') }}">Thêm</a>
              </li>
            </ol>
          </nav>
        </div>
        <div class="card-body">
          <form action="{{ isset($voucher->id) ? route('voucher.update',$voucher->id) : route('voucher.store') }}"  method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="mb-3">
              <label class="form-label" for="name">Tên voucher</label>
              <input type="text" name="name" 
                value="{{ old('name', $voucher->name ?? '') }}" 
                class="form-control @error('name') is-invalid @enderror" 
                id="name" placeholder="Giảm giá 15k" />
              @if($errors->has('name'))
                <span class="text-danger"> {{ $errors->first('name') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="discount">Số tiền giảm</label>
              <input type="number" 
                value="{{ old('discount', $voucher->discount ?? '') }}"  
                name="discount" 
                class="form-control @error('discount') is-invalid @enderror" 
                id="discount" placeholder="15000" />
              @if($errors->has('discount'))
                <span class="text-danger"> {{ $errors->first('discount') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="min">Giá trị đơn hàng tối thiểu</label>
              <input type="number" 
                value="{{ old('min', $voucher->min ?? '') }}"  
                name="min" 
                class="form-control @error('min') is-invalid @enderror" 
                id="min" placeholder="15000" />
              @if($errors->has('min'))
                <span class="text-danger"> {{ $errors->first('min') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="min">Thời gian</label>
              <div class="row">
                <div class="form-group col-md-3">
                  <label for="inputEmail4">Bắt đầu</label>
                  <input type="datetime-local" class="form-control" name="start" min="{{Carbon\Carbon::now()->format('Y-m-d\TH:i') }}"> 
                </div>
                <div class="form-group col-md-3">
                  <label for="inputPassword4">Kết thúc</label>
                  <input type="datetime-local" class="form-control" name="end" min="{{Carbon\Carbon::now()->addDays(2)->format('Y-m-d\TH:i') }}">
                </div>
              </div>
            </div>
            <div class="mb-3">
              <label class="form-label" for="description">Mô tả ngắn</label>
              <input type="text" 
                value="{{ old('description', $voucher->description ?? '') }}"  
                name="description" 
                class="form-control @error('description') is-invalid @enderror" 
                id="description" placeholder="001" />
              @if($errors->has('description'))
                <span class="text-danger"> {{ $errors->first('description') }}</span>
              @endif
            </div>
            <div class="form-check form-switch mb-3 d-none">
              <input class="form-check-input" type="checkbox" name="status" id="status" checked>
              <label class="form-check-label" for="status" >Hiển thị</label>
              @if($errors->has('status'))
                <span class="text-danger"> {{ $errors->first('status') }}</span>
              @endif
            </div>
            <button type="submit" class="btn btn-primary">Thêm</button>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection
@section('page-script')
  <script>
      let currentDate = new Date().toISOString().slice(0, -8); //yyyy-MM-ddThh:mm
      document.querySelector("#start").min = currentDate;
  </script>
@endsection