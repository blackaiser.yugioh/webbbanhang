@extends('layouts/contentNavbarLayout')

@section('title', 'Sản phẩm')

@section('content')

<!-- form search -->
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <h5 class="card-header">Tìm kiếm</h5>
      <div class="card-body demo-vertical-spacing demo-only-element col-7">
        <form method="GET" class="d-flex">
          <div class="input-group">
            <select class="form-select" name="type" style="max-width: 200px;">
              <option {{ request('type') == 1 ? "selected" : "" }} value="1">Tên sản phẩm</option>
            </select>
            <input value="{{request('k')}}" type="text" name="k" class="form-control" placeholder="Nhập từ khóa tìm kiếm....">
          </div>
          <button class="mx-1 btn btn-outline-primary" style="width: 150px;" type="submit">Tìm kiếm</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Bordered Table -->
<div class="card">
  <h5 class="card-header">Quản lý sản phẩm</h5>
  <div class="card-body">
    <div class="table-responsive text-nowrap">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Sản phẩm</th>
            <th>Danh mục</th>
            <th>Giá</th>
            <th>Kho</th>
            <th>Đã bán</th>
            <th>Review/điểm</th>
            <th>Trạng thái</th>
            <!-- <th>Hoạt động</th> -->
          </tr>
        </thead>
        <tbody>
          @foreach($products as $product)
          <tr>
            <td>
              <div class="product d-flex">
                <img src="{{ $product->thumbnail }}" alt="" style="width: 70px; height:70px; min-width: 70px">
                <span class="product-name p-1 d-flex align-items-center" title="{{ $product->name }}">
                  {{ $product->name }}
                </span>
              </div>
            </td>
            <td>{{ $product->category->name }}</td>
            <td>@money_vn($product->price)</td>
            <td>{{ $product->quantity }}</td>
            <td>{{ $product->buycnt }}</td>
            <td>{{ $product->review }}/{{ $product->point }}</td>
            <td>
              <div class="d-flex justify-content-between align-items-center">
                @if($product->status === 1)
                  <span class="badge bg-label-success me-1">Hiển thị</span>
                @else 
                  <span class="badge bg-label-secondary me-1">Ẩn</span>
                @endif
                <div class="dropdown">
                  <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('product.edit', ['id' => $product->id]) }}"><i class="bx bx-edit-alt me-1"></i>Sửa</a>
                    <a class="dropdown-item" href="{{ route('product.hide', ['id' => $product->id]) }}"><i class="bx bx-hide me-1"></i>@if($product->status === 1) Ẩn @else Hiển thị @endif</a>
                  </div>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="d-flex mt-3 justify-content-center">
      {!! $products->links() !!}
    </div>
  </div>
</div>
<!--/ Bordered Table -->
@endsection