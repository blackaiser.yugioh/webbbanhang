@extends('layouts/contentNavbarLayout')

@section('title', 'Liên hệ')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <h5 class="card-header">Tìm kiếm</h5>
      <div class="card-body demo-vertical-spacing demo-only-element col-7">
        <form method="GET" class="d-flex">
          <div class="input-group">
            <input value="{{request('k')}}" type="text" name="k" class="form-control" placeholder="Nhập từ khóa tìm kiếm....">
          </div>
          <button class="mx-1 btn btn-outline-primary" style="width: 150px;" type="submit">Tìm kiếm</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="card">
  <h5 class="card-header">Quản lý liên hệ</h5>
  <div class="card-body">
    <div class="table-responsive text-nowrap" style="padding-bottom: 100px;">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>STT</th>
            <th>Tên</th>
            <th>Email</th>
            <th>Số điện thoại</th>
            <th>Lời nhắn</th>
          </tr>
        </thead>
        <tbody>
          @foreach($contacts as $k => $contact)
            <tr>
              <td>{{ $k+1 }}</td>
              <td>{{ $contact->name }}</td>
              <td>{{ $contact->email }}</td>
              <td>{{ $contact->phone }}</td>
              <td>{{ $contact->message }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="d-flex mt-3 justify-content-center">
      {!! $contacts->links() !!}
    </div>
  </div>
</div>
@endsection