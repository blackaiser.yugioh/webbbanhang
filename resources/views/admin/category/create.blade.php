@extends('layouts/contentNavbarLayout')

@section('title', 'Thêm danh mục sản phẩm')
@section('content')

<div class="row">
    <div class="col-xl">
      <div class="card mb-4">
        <div class="card-header d-flex  align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{{ route('category') }}">Danh mục sản phẩm</a>
              </li>
              <li class="breadcrumb-item active">
                <a href="{{ route('category-create') }}">Thêm</a>
              </li>
            </ol>
          </nav>
        </div>
        <div class="card-body">
          <form action="{{ isset($cat->id) ? route('category.update',$cat->id) : route('category-store') }}"  method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="mb-3">
              <label class="form-label" for="name">Tên danh mục</label>
              <input type="text" name="name" 
                value="{{ old('name', $cat->name ?? '') }}" 
                class="form-control @error('name') is-invalid @enderror" 
                id="name" placeholder="Laptop" />
              @if($errors->has('name'))
                <span class="text-danger"> {{ $errors->first('name') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label class="form-label" for="code">Mã danh mục</label>
              <input type="text" 
                value="{{ old('code', $cat->code ?? '') }}"  
                name="code" 
                class="form-control @error('code') is-invalid @enderror" 
                id="code" placeholder="001" />
              @if($errors->has('code'))
                <span class="text-danger"> {{ $errors->first('code') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label for="defaultSelect" class="form-label">Danh mục cha</label>
              <select id="defaultSelect" class="form-select" name="parent">
                <option value="0">Chọn danh mục cha</option>
                @foreach($cats as $item)
                  <option value="{{$item->id}}"  {{ old('parent', $cat->parent ?? '' ) === $item->id ? "selected" : "" }}>{{$item->name}}</option>
                @endforeach
              </select>
              <div id="defaultFormControlHelp" class="form-text">Vui lòng không chọn nếu là danh mục cha.</div>
              @if($errors->has('parent'))
                <span class="text-danger"> {{ $errors->first('parent') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label for="thumbnail" class="thumbnail">Ảnh đại diện</label>
              <input class="form-control" name="thumbnail" id="thumbnail" type="file"accept="image/*">
              @if($errors->has('thumbnail'))
                <span class="text-danger"> {{ $errors->first('thumbnail') }}</span>
              @endif
              <img id="imgThumbnail" class="@if(($cat->id ?? 0) > 0) d-block mt-1 @else d-none @endif" height="100" src="{{ $cat->thumbnail ?? '' }}" alt="your image" />
            </div>
            <div class="form-check form-switch mb-3">
              <input class="form-check-input" type="checkbox" name="hide" id="hide" checked>
              <label class="form-check-label" for="hide" >Hiển thị</label>
              @if($errors->has('hide'))
                <span class="text-danger"> {{ $errors->first('hide') }}</span>
              @endif
            </div>
            <button type="submit" class="btn btn-primary">Thêm</button>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection
@section('page-script')
  <script>
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#imgThumbnail').attr('src', e.target.result);
              $('#imgThumbnail').attr("class","d-block m-1");
          }

          reader.readAsDataURL(input.files[0]);
      }
    }

    $("#thumbnail").change(function(){
      readURL(this);
    });
  </script>
@endsection