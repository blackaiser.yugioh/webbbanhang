@extends('layouts/contentNavbarLayout')

@section('title', 'Dashboard - Analytics')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/apex-charts/apex-charts.css')}}">
@endsection

@section('vendor-script')
{{-- <script src="{{asset('assets/vendor/libs/apex-charts/apexcharts.js')}}"></script> --}}
@endsection

@section('page-script')
{{-- <script src="{{asset('assets/js/dashboards-analytics.js')}}"></script> --}}
@endsection

@section('content')
<div class="row">
  <div class="col-lg-8 mb-4 order-0">
    <div class="card">
      <div class="d-flex align-items-end row">
        <div class="col-sm-7">
          <div class="card-body">
            <h5 class="card-title text-primary">Chúc mừng! 🎉</h5>
            <p class="mb-4">Hôm nay bạn đã có <span class="fw-bold">{{ $totalOorderToday }}</span> đơn hàng.</p>

            <a href="javascript:;" class="btn btn-sm btn-outline-primary">Đơn hàng</a>
          </div>
        </div>
        <div class="col-sm-5 text-center text-sm-left">
          <div class="card-body pb-0 px-0 px-md-4">
            <img src="{{asset('assets/img/illustrations/man-with-laptop-light.png')}}" height="140" alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png" data-app-light-img="illustrations/man-with-laptop-light.png">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-4 order-1">
    <div class="row">
      <div class="col-lg-6 col-md-12 col-6 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar flex-shrink-0">
                <img src="{{asset('assets/img/icons/unicons/chart-success.png')}}" alt="chart success" class="rounded">
              </div>
              <div class="dropdown">
                <button class="btn p-0" type="button" id="cardOpt3" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt3">
                  <a class="dropdown-item" href="javascript:void(0);">View More</a>
                  <a class="dropdown-item" href="javascript:void(0);">Delete</a>
                </div>
              </div>
            </div>
            <span class="fw-semibold d-block mb-1">Người dùng mới</span>
            <h3 class="card-title mb-2">{{$userToday}}</h3>
            @if($compareUser < 0 )
              <small class="text-danger fw-semibold"><i class='bx bx-down-arrow-alt'></i> {{$compareUser * 100}}%</small>
            @elseif($compareUser > 0)
              <small class="text-success text-nowrap fw-semibold"><i class='bx bx-chevron-up'></i> {{$compareUser * 100}} %</small>
            @endif
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12 col-6 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar flex-shrink-0">
                <img src="{{asset('assets/img/icons/unicons/wallet-info.png')}}" alt="Credit Card" class="rounded">
              </div>
            </div>
            <span>Tiền thanh toán online</span>
            <h3 class="card-title text-nowrap mb-1">@money_vn($payOnline)</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Top tháng -->
  <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
    <div class="card">
      <div class="row row-bordered g-0">
        <div class="col-md-12">
          <h5 class="card-header m-0 me-2 pb-3">Thống kê doanh thu</h5>
          <div class="col-md-4 m-2">
            <form action="{{ route('dashboard') }}" class="d-flex">
              <div class="input-group flex-nowrap">
                <span class="input-group-text" id="addon-wrapping">Bắt đầu</span>
                <input value="{{request('start')}}" max="<?= date('Y-m-d', time()); ?>" type="date" class="form-control" name="start">
              </div>
              <div class="input-group flex-nowrap mx-2">
                <span class="input-group-text" id="addon-wrapping">Kết thúc</span>
                <input value="{{request('end')}}" max="<?= date('Y-m-d', time()); ?>" type="date" class="form-control" name="end">
              </div>
              <button type="submit" class="btn btn-primary mx-2">Search</button>
            </form>
          </div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Ngày</th>
                <th>Số đơn hàng</th>
                <th>Số lượng sản phẩm</th>
                <th>Dự kiến</th>
                <th>Thực thu</th>
                <th>Chi tiết</th>
              </tr>
            </thead>
            <tbody>
              @php 
              $total = 0;
              $total2= 0;
              @endphp
              @foreach($datePeriod as $datePeriodRow)
                @php 
                  $date = $datePeriodRow->format('d/m/Y');
                  $time = strtotime($datePeriodRow->format('d-m-Y'));
                  $money = 0;
                  $money2 = 0;
                  $totalProduct = 0;
                  if(!empty($orders[$date])){
                    foreach($orders[$date] as $order){
                      if(in_array($order->status, [0,1,2,3])) {
                        $money += $order->total_pay;
                      }
                      if(in_array($order->status, [1,3])) {
                        $money2 += $order->total_pay;
                      }
                      foreach ($order->orderProducts as $product) {
                        $totalProduct += $product->quantity;
                      }
                    }
                    $total += $money;
                    $total2 += $money2;
                  }
                @endphp
                <tr>
                  <td>{{ $datePeriodRow->format('d/m/Y') }} {{$time}}</td>
                  <td>{{ !empty($orders[$date]) ? count($orders[$date]) : 0}}</td>
                  <td>{{ $totalProduct }}</td>
                  <td>@money_vn($money) </td>
                  <td>@money_vn($money2) </td>
                  <td><a href="{{ route('dashboard.detail', ['date' => $time]) }}">Chi tiết</a></td>
                </tr>                
              @endforeach
              <tr>
                <th colspan="2" class="text-end text-danger fs-5 fw-bolder">Tổng doanh thu: </th>
                <td class="text-danger fs-5 fw-bolder">@money_vn($total)</td>
                <td class="text-danger fs-5 fw-bolder">@money_vn($total2)</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-md-8 col-lg-4 order-3 order-md-2">
    <div class="row">
      <div class="col-6 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="card-title d-flex align-items-start justify-content-between">
              <div class="avatar flex-shrink-0">
                <img src="{{asset('assets/img/icons/unicons/paypal.png')}}" alt="Credit Card" class="rounded">
              </div>
            </div>
            <span class="d-block mb-1">Thu Nhập</span>
            <h3 class="card-title text-nowrap mb-2">@money_vn($totalMoneyToday)</h3>
            @if($compare < 0 )
              <small class="text-danger fw-semibold"><i class='bx bx-down-arrow-alt'></i> {{$compare * 100}}%</small>
            @elseif($compare > 0)
              <small class="text-success text-nowrap fw-semibold"><i class='bx bx-chevron-up'></i> {{$compare * 100}} %</small>
            @endif
          </div>
        </div>
      </div>
      <!-- </div>
    <div class="row"> -->
    </div>
  </div>
  <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
    <div class="card">
      <div class="row row-bordered g-0">
        <div class="col-md-12">
          <h5 class="card-header m-0 me-2 pb-3">Top sản phẩm bán chạy {{ request('time') }}</h5>
          <div class="col-md-4 m-2">
            <form action="{{ route('dashboard') }}" class="d-flex">
              <div class="input-group flex-nowrap">
                <span class="input-group-text" id="addon-wrapping">Thời gian</span>
                <input value="{{request('time')}}" max="<?= date('Y-m-d', time()); ?>" type="month" class="form-control" name="time">
              </div>
              <button type="submit" class="btn btn-primary mx-2">Search</button>
            </form>
          </div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Tên sản phẩm</th>
                <th>Số đơn hàng</th>
                <th>Số sản phẩm</th>
                <th>Tồn kho</th>
                <th>Thu nhập</th>
              </tr>
            </thead>
            <tbody>
              @foreach($products as $k=>$v)
              @php
                $total = 0;
                $quantity = 0;
                foreach ($v as $product) {
                  $total += ($product->quantity * $product->pay_price);
                  $quantity += $product->product->quantity;
                }
              @endphp
                <tr>
                  <td>{{ $k }}</td>
                  <td>{{ $v->count }}</td>
                  <td>{{ $v->total_quantity }}</td>
                  <td>{{ $quantity }}</td>
                  <td> @money_vn($total) </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!--/ Total Revenue -->
</div>
@endsection
