@extends('layouts/contentNavbarLayout')

@section('title', 'Dashboard - Analytics')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/apex-charts/apex-charts.css')}}">
@endsection

@section('vendor-script')
{{-- <script src="{{asset('assets/vendor/libs/apex-charts/apexcharts.js')}}"></script> --}}
@endsection

@section('page-script')
{{-- <script src="{{asset('assets/js/dashboards-analytics.js')}}"></script> --}}
@endsection

@section('content')
<div class="row">
  <div class="col-lg-8 mb-4 order-0">
    <div class="card">
      <div class="d-flex align-items-end row">
        <div class="col-sm-7">
          <div class="card-body">
            <h5 class="card-title text-primary">Chúc mừng! 🎉</h5>
            <p class="mb-4">Hôm nay bạn đã có <span class="fw-bold">0</span> đơn hàng.</p>

            <a href="javascript:;" class="btn btn-sm btn-outline-primary">Đơn hàng</a>
          </div>
        </div>
        <div class="col-sm-5 text-center text-sm-left">
          <div class="card-body pb-0 px-0 px-md-4">
            <img src="{{asset('assets/img/illustrations/man-with-laptop-light.png')}}" height="140" alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png" data-app-light-img="illustrations/man-with-laptop-light.png">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Total Revenue -->
  <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
    <div class="card">
      <div class="row row-bordered g-0">
        <div class="col-md-12">
          <h5 class="card-header m-0 me-2 pb-3">Thống kê doanh thu ngày {{ $date->format('d/m/Y') }}</h5>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Tên sản phẩm</th>
                <th>Số đơn hàng</th>
                <th>Số sản phẩm</th>
                <th>Thu nhập</th>
              </tr>
            </thead>
            <tbody>
              @foreach($products as $k=>$v)
              @php
                $total = 0;
                foreach ($v as $product) {
                  $total += ($product->quantity * $product->pay_price);
                }
              @endphp
                <tr>
                  <td>{{ $k }}</td>
                  <td>{{ $v->count }}</td>
                  <td>{{ $v->total_quantity }}</td>
                  <td> @money_vn($total) </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
