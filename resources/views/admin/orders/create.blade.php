@extends('layouts/contentNavbarLayout')

@section('title', 'Thêm danh mục sản phẩm')
@section('content')

<div class="row">
    <div class="col-xl">
      <div class="card mb-4">
        <div class="card-header d-flex  align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{{ route('product') }}">Danh mục sản phẩm</a>
              </li>
              <li class="breadcrumb-item active">
                <a href="{{ route('product.create') }}">Thêm</a>
              </li>
            </ol>
          </nav>
        </div>
        <form action="{{ isset($product->id) ? route('product.update',$product->id) : route('product.store') }}"  method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="card-body row">
            <div class="col-md-6 col-12">
              <div class="mb-3">
                <label class="form-label" for="name">Tên sản phẩm</label>
                <input type="text" name="name" 
                  value="{{ old('name', $product->name ?? '') }}" 
                  class="form-control @error('name') is-invalid @enderror" 
                  id="name" placeholder="Laptop" />
                @if($errors->has('name'))
                  <span class="text-danger"> {{ $errors->first('name') }}</span>
                @endif
              </div>
              <div class="mb-3">
                <label class="form-label" for="price">Giá thành</label>
                <input type="number" 
                  value="{{ old('price', $product->price ?? '') }}"  
                  name="price" 
                  class="form-control @error('price') is-invalid @enderror" 
                  id="price" placeholder="100,000" />
                @if($errors->has('price'))
                  <span class="text-danger"> {{ $errors->first('price') }}</span>
                @endif
              </div>
              <div class="mb-3">
                <label class="form-label" for="quantity">Kho</label>
                <input type="number" 
                  value="{{ old('quantity', $product->quantity ?? '') }}"  
                  name="quantity" 
                  class="form-control @error('quantity') is-invalid @enderror" 
                  id="quantity" placeholder="100" />
                @if($errors->has('quantity'))
                  <span class="text-danger"> {{ $errors->first('quantity') }}</span>
                @endif
              </div>
              <div class="mb-3">
                <label for="defaultSelect" class="form-label">Danh mục sản phẩm</label>
                <select id="defaultSelect" class="form-select  @error('category_id') is-invalid @enderror" name="category_id">
                  <option value="">Chọn danh mục sản phẩm</option>
                  @foreach($cats as $item)
                    <option value="{{$item->id}}"  {{ old('category_id', $product->category_id ?? '' ) === $item->id ? "selected" : "" }}>{{$item->name}}</option>
                  @endforeach
                </select>
                @if($errors->has('category_id'))
                  <span class="text-danger"> {{ $errors->first('category_id') }}</span>
                @endif
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="mb-3">
                <label for="thumbnail" class="thumbnail">Ảnh đại diện</label>
                <input class="form-control" name="thumbnail" id="thumbnail" type="file"accept="image/*">
                @if($errors->has('thumbnail'))
                  <span class="text-danger"> {{ $errors->first('thumbnail') }}</span>
                @endif
                <img id="imgThumbnail" class="@if(($product->id ?? 0) > 0) d-block mt-1 @else d-none @endif " height="100" src="{{ $product->thumbnail ?? '' }}" alt="your image" />
              </div>
              <div class="mb-3">
                <label for="images" class="thumbnail">Ảnh chi tiết sản phẩm</label>
                <input class="form-control" multiple name="images[]" id="images" type="file"accept="image/*">
                @if($errors->has('images'))
                  <span class="text-danger"> {{ $errors->first('images') }}</span>
                @endif
                <div class="gallery">
                  @if($product->id && count($product->images) > 0)
                    @foreach($product->images as $image)
                      <img class="mt-1 " height="100" src="{{ $image->url }}" alt="your image" />
                    @endforeach
                  @endif
                </div>
                <!-- <img id="imgThumbnail" class="@if(($product->id ?? 0) > 0) d-block mt-1 @else d-none @endif" height="100px" src="{{ $product->thumbnail ?? '' }}" alt="your image" /> -->
              </div>
            </div>
            <div class="col-12">
              <div class="mb-3">
                <label class="form-label" for="short">Thông số kỹ thuật</label>
                <textarea 
                  name="short" 
                  class="form-control @error('short') is-invalid @enderror" 
                  id="editor">
                  {!! old('short', $product->short) !!}
                </textarea>
                @if($errors->has('short'))
                  <span class="text-danger"> {{ $errors->first('short') }}</span>
                @endif
              </div>
              <div class="mb-3">
                <label class="form-label" for="description">Mô tả sản phẩm</label>
                <textarea name="description" 
                  class="form-control @error('description') is-invalid @enderror" 
                  id="editor-1">
                  {!! old('description', $product->description) !!}
                </textarea>
                @if($errors->has('description'))
                  <span class="text-danger"> {{ $errors->first('description') }}</span>
                @endif
              </div>
              <div class="form-check form-switch mb-3">
                <input class="form-check-input" type="checkbox" name="status" id="status" {{ ($product->status === 1) ? 'checked' : '' }}>
                <label class="form-check-label" for="status" >Hiển thị</label>
                @if($errors->has('status'))
                  <span class="text-danger"> {{ $errors->first('status') }}</span>
                @endif
              </div>
            </div>
            <button type="submit" style="width: 100px;" class="m-2 btn btn-primary">Thêm</button>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection
@section('page-script')
  <script>
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#imgThumbnail').attr('src', e.target.result);
              $('#imgThumbnail').attr("class","d-block m-1 border-radius-5");
          }

          reader.readAsDataURL(input.files[0]);
      }
    }

    $("#thumbnail").change(function(){
      readURL(this);
    });

    //preview multiple image
    var imagesPreview = function(input, placeToInsertImagePreview) {

    if (input.files) {
        var filesAmount = input.files.length;

        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();

            reader.onload = function(event) {
                $($.parseHTML('<img>')).attr('src', event.target.result).attr('class','m-1').appendTo(placeToInsertImagePreview);
            }

            reader.readAsDataURL(input.files[i]);
        }
    }

    };

    $('#images').on('change', function() {
    imagesPreview(this, 'div.gallery');
    });
  </script>
@endsection