@extends('layouts/contentNavbarLayout')

@section('title', 'Chi tiết đơn hàng')

@section('content')

<div class="card">
  <h5 class="card-header fs-3">Thông tin đơn hàng</h5>
  <div class="card-body" style="line-height: 30px;">
      <div>Mã đơn hàng: <strong>{{ $order->code }}</strong></div>
      <div>Người mua: <strong>{{ $order->user->name }}</strong></div>
      <div>SĐT: <strong>{{ $order->phone }}</strong></div>
      <div>Địa chỉ nhận hàng: <strong>{{ $order->address }}</strong></div>
      <hr>
      <h5 class="fs-3">Thanh toán</h5>
      <div>Tổng tiền: <strong>@money_vn($order->total)</strong></div>
      <div>Khuyến mại: <strong>@money_vn(-$order->sale)</strong></div>
      <div>Voucher: <strong>@money_vn(-$order->voucher_discount)</strong></div>
      <div>Ship: <strong>@money_vn($order->ship)</strong></div>
      <div>Thanh toán: <strong class="text-danger">@money_vn($order->total_pay)</strong></div>
      <div class="d-flex">Trạng thái đơn hàng: 
        <strong class="text-danger mx-1"> {{ $order->status_des }}</strong> 
        @if($order->status != 3)
          <button type="button" class="mx-3 btn btn-primary"  data-bs-toggle="modal" data-bs-target="#exampleModal">
            <i class="fa-regular fa-pen-to-square"></i> 
            Cập nhật
          </button>
        @endif
      </div>
      <div>Phương thức thanh toán: <strong class="text-danger">{{ $order->payment_type == 0 ? 'Thanh toán khi nhận hàng' : 'VNPay' }}</strong></div>
  </div>
</div>
<!-- Bordered Table -->
<div class="card mt-3">
  <h5 class="card-header fs-3">Danh sách sản phẩm</h5>
  <div class="card-body">
    <div class="table-responsive text-nowrap">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Tên sản phẩm</th>
            <th>Ảnh sản phẩm</th>
            <th>Số lượng</th>
            <th>Giá sản phẩm</th>
            <th>Giá bán</th>
          </tr>
        </thead>
        <tbody>
          @foreach($order->orderProducts as $product)
          <tr>
            <td><img src="{{ $product->thumbnail }}" height="50" width="50" alt=""></td>
            <td>{{ $product->product_name }}</td>
            <td>{{ $product->quantity }}</td>
            <td>@money_vn( $product->price)</td>
            <td class="text-danger">@money_vn( $product->pay_price)</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <modal-update-status :status="{{ json_encode($status) }}" :order="{{ json_encode($order) }}"></modal-update-status>
</div>
<!--/ Bordered Table -->
@endsection