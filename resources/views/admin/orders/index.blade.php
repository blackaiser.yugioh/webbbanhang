@extends('layouts/contentNavbarLayout')

@section('title', 'Đơn hàng')

@section('content')

<!-- form search -->
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <h5 class="card-header">Tìm kiếm</h5>
      <div class="card-body demo-vertical-spacing demo-only-element col-7">
        <form method="GET" class="d-flex">
          <div class="form-group col-md-3 mx-1">
            <label for="inputEmail4">Trạng thái đơn hàng</label>
            <select name="status" class="form-select" aria-label="Default select example">
              <option {{ request('status') == '10' ? 'selected' : '' }} value="10">Tất cả</option>
              <option {{ request('status') == '0' ? 'selected' : '' }} value="0">Chờ thanh toán</option>
              <option {{ request('status') == '1' ? 'selected' : '' }} value="1">Đã thanh toán</option>
              <option {{ request('status') == '2' ? 'selected' : '' }} value="2">Đang giao hàng</option>
              <option {{ request('status') == '3' ? 'selected' : '' }} value="3">Đã hoàn thành</option>
              <option {{ request('status') == '4' ? 'selected' : '' }} value="4">Đã hủy</option>
              <option {{ request('status') == '5' ? 'selected' : '' }} value="5">Trả hàng/hoàn tiền</option>
              <option {{ request('status') == '6' ? 'selected' : '' }} value="6">Thanh toán không thành công</option>
          </select>
          </div>
          <div class="form-group col-md-3 mx-1">
            <label for="inputEmail4">Mã đơn hàng</label>
            <input value="{{request('code')}}" type="text" name="code" class="form-control" placeholder="Nhập mã đơn hàng">
          </div>
          <div class="form-group col-md-3 mx-1">
            <label for="inputEmail4">Số điện thoại</label>
            <input value="{{request('phone')}}" type="text" name="phone" class="form-control" placeholder="Số điện thoại đặt hàng">
          </div>
          <div class="form-group col-md-3 mx-1">
            <label for="inputEmail4">Tên người mua</label>
            <input value="{{request('name')}}" type="text" name="name" class="form-control" placeholder="Nhập mã đơn hàng">
          </div>
          <div class="form-group col-md-3 mx-1">
            <label for="inputEmail4">Ngày mua</label>
            <input value="{{request('date')}}" type="date" name="date" class="form-control" placeholder="Nhập mã đơn hàng">
          </div>
          <button class="mx-1 btn btn-outline-primary" style="width: 150px;" type="submit">Tìm kiếm</button>
        </form>
        
      </div>
    </div>
  </div>
</div>
<!-- Bordered Table -->
<div class="card">
  <h5 class="card-header">Quản lý đơn hàng</h5>
  <div class="card-body">
    <div class="table-responsive text-nowrap">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Mã đơn hàng</th>
            <th>Tên/SĐT</th>
            <th class="d-none">Địa chỉ</th>
            <th>Voucher</th>
            <th>Khuyến mại</th>
            <th>Thanh toán</th>
            <th>Hình thức thanh toán</th>
            <th>Ngày đặt</th>
            <th>Trạng thái</th>
          </tr>
        </thead>
        <tbody>
          @foreach($orders as $order)
          <tr>
            <td>{{ $order->code }}</td>
            <td>{{ $order->user->name }}<br>{{ $order->phone }}</td>
            <td class="d-none">{{ $order->address }}</td>
            <td>@money_vn(-$order->voucher_discount)</td>
            <td>@money_vn(-$order->sale)</td>
            <td style="color: red; font-weight: 600;">@money_vn($order->total_pay)</td>
            <td>
              {{ $order->payment_type == 0 ? 'COD' : 'VNPay' }}
            </td>
            <td>{{ $order->created_at }}</td>
            <td >
              <div class="d-flex justify-content-between ">
                <div class="{{ $order->status == 4 ? 'text-danger' : '' }}" {{$order->status == 3 ? 'text-success' : ''}}>
                  {{config('constants.order_status')[$order->status]}}
                </div>
                <div class="dropdown">
                  <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('order.show', ['id' => $order->id]) }}"><i class="bx bx-edit-alt me-1"></i>Chi tiết</a>
                  </div>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="d-flex mt-3 justify-content-center">
      {!! $orders->links() !!}
    </div>
  </div>
</div>
<!--/ Bordered Table -->
@endsection