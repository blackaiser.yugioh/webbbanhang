@extends('layouts/contentNavbarLayout')

@section('title', 'Quản lý bài viết')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <h5 class="card-header">Tìm kiếm</h5>
      <div class="card-body demo-vertical-spacing demo-only-element col-7">
        <form method="GET" class="d-flex">
          <div class="input-group">
            <input value="{{request('k')}}" type="text" name="k" class="form-control" placeholder="Nhập từ khóa tìm kiếm....">
          </div>
          <button class="mx-1 btn btn-outline-primary" style="width: 150px;" type="submit">Tìm kiếm</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="card">
  <h5 class="card-header">Quản lý danh mục sản phẩm</h5>
  <div class="card-body">
    <div class="table-responsive text-nowrap">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>STT</th>
            <th>Tiêu đề</th>
            <th>Trạng thái</th>
            <th>Hoạt động</th>
          </tr>
        </thead>
        <tbody>
          @foreach($posts as $post) 
          <tr>
            <td><strong>{{ $loop->index +1 }}</strong></td>
            <td>{{ $post->title }}</td>
            <td>
              @if($post->status === 1)
                <span class="badge bg-label-success me-1">Hiển thị</span>
              @else 
                <span class="badge bg-label-secondary me-1">Ẩn</span>
              @endif
            </td>
            <td>
              <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="{{ route('post.edit', ['id' => $post->id]) }}"><i class="bx bx-edit-alt me-1"></i>Chi tiết</a>
                  <a class="dropdown-item" href="{{ route('post.hide', ['id' => $post->id]) }}"><i class="bx bx-hide me-1"></i>@if($post->status === 1) Ẩn @else Hiển thị @endif</a>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="d-flex mt-3 justify-content-center">
      {!! $posts->links() !!}
    </div>
  </div>
</div>
@endsection