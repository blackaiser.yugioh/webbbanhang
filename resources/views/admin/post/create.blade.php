@extends('layouts/contentNavbarLayout')

@section('title', 'Thêm danh mục sản phẩm')
@section('content')

<div class="row">
    <div class="col-xl">
      <div class="card mb-4">
        <div class="card-header d-flex  align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{{ route('post') }}">Danh mục tin tức</a>
              </li>
              <li class="breadcrumb-item active">
                <a href="{{ route('post.create') }}">Thêm</a>
              </li>
            </ol>
          </nav>
        </div>
        <div class="card-body">
          <form action="{{ isset($post->id) ? route('post.update',$post->id) : route('post.store') }}"  method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="mb-3">
              <label class="form-label" for="title">Tiêu đề</label>
              <input type="text" name="title" 
                value="{{ old('title', $post->title ?? '') }}" 
                class="form-control @error('title') is-invalid @enderror" 
                id="title" placeholder="Laptop" />
              @if($errors->has('title'))
                <span class="text-danger"> {{ $errors->first('title') }}</span>
              @endif
            </div>
            <div class="mb-3">
              <label for="image" class="thumbnail">Ảnh đại diện</label>
              <input class="form-control" name="image" id="thumbnail" type="file"accept="image/*">
              @if($errors->has('image'))
                <span class="text-danger"> {{ $errors->first('image') }}</span>
              @endif
              <img id="imgThumbnail" class="@if(($post->id ?? 0) > 0) d-block mt-1 @else d-none @endif" height="100" src="{{ $post->image ?? '' }}" alt="your image" />
            </div>
            <div class="mb-3">
              <label class="form-label" for="description">Chi tiết</label>
              <textarea name="description" 
                class="form-control @error('description') is-invalid @enderror" 
                id="editor-1">
                {!! old('description', $post->description) !!}
              </textarea>
              @if($errors->has('description'))
                <span class="text-danger"> {{ $errors->first('description') }}</span>
              @endif
            </div>
            <button type="submit" class="btn btn-primary">@if(!isset($post->id)) Thêm @else Cập nhật @endif</button>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection