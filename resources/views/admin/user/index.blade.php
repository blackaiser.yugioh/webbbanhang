@extends('layouts/contentNavbarLayout')

@section('title', 'Người dùng')

@section('content')

<!-- form search -->
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <h5 class="card-header">Tìm kiếm</h5>
      <div class="card-body demo-vertical-spacing demo-only-element col-7">
        <form method="GET" class="d-flex">
          <div class="input-group">
            <select class="form-select" name="type" style="max-width: 200px;">
              <option {{ request('type') == 1 ? "selected" : "" }} value="1">Tên tài khoản</option>
              <option {{ request('type') == 2 ? "selected" : "" }} value="2">Email</option>
            </select>
            <input value="{{request('k')}}" type="text" name="k" class="form-control" placeholder="Nhập từ khóa tìm kiếm....">
          </div>
          <button class="mx-1 btn btn-outline-primary" style="width: 150px;" type="submit">Tìm kiếm</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- table user -->
<div class="card">
  <h5 class="card-header">Quản lý người dùng</h5>
  <div class="card-body">
    <div class="table-responsive text-nowrap">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>STT</th>
            <th>Tên tài khoản</th>
            <th>Email</th>
            <th>Số đơn hàng</th>
            <th>Số bài đánh giá</th>
            <th>Trạng thái</th>
            <th class="text-center">Thao tác</th>
          </tr>
        </thead>
        <tbody>
         @foreach($users as $user) 
          <tr>
            <td><strong>{{ $loop->index +1 }}</strong></td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>0</td>
            <td>0</td>
            
            <td >
              <div class="d-flex justify-content-between align-items-center">
                @if($user->lock === 1)
                  <span class="badge bg-label-success me-1">Đang hoạt động</span>
                @else 
                  <span class="badge bg-label-secondary me-1">Bị khóa</span>
                @endif
                <div class="dropdown d-none">
                  <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('user.lock', ['id' => $user->id]) }}"><i class="bx bx-edit-alt me-1"></i>Sửa</a>
                    <a class="dropdown-item" href="{{ route('user.lock', ['id' => $user->id]) }}"><i class="bx bx-hide me-1"></i>@if($user->lock === 1) Khóa @else Mở khóa @endif</a>
                  </div>
                </div>
              </div>
            </td>
            <td>
              <a class="dropdown-item" href="{{ route('user.lock', ['id' => $user->id]) }}">
                <i class="bx me-1 @if($user->lock === 0) bx-lock-open-alt @else bx-lock @endif"></i>
                @if($user->lock === 1) Khóa @else Mở khóa @endif
              </a>
            </td>
          </tr>
         @endforeach
        </tbody>
      </table>
    </div>
    <div class="d-flex mt-3 justify-content-center">
      {!! $users->links() !!}
    </div>
  </div>
</div>
@endsection