@extends('layouts/layout-user' )
@section('title', 'Giỏ hàng')
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')
<div class="cart">
    <cart
        :carts="{{ json_encode($carts) }}">
    </cart>
</div>
@endsection