@extends('layouts/layout-user' )
@section('title', 'Checkout')
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')
<div class="cart">
    <checkout 
        :user="{{ json_encode($user) }}"
        :voucher="{{ json_encode($voucher) }}"
        :products="{{ json_encode($carts) }}">
    </checkout>
</div>
@endsection