@extends('layouts/layout-user' )
@section('title', 'Tin tức')
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="pt-3 d-flex">
        <div id="carouselExample" class="carousel slide col-lg--8 col-md-8 col-12 py-1" data-bs-ride="carousel">
            <ol class="carousel-indicators">
                <li data-bs-target="#carouselExample" data-bs-slide-to="0" class="active"></li>
                <li data-bs-target="#carouselExample" data-bs-slide-to="1"></li>
                <li data-bs-target="#carouselExample" data-bs-slide-to="2"></li>
            </ol> 
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-84354b31222d3e8c4e25b02bf791b65d_xxhdpi" alt="First slide" />
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-84354b31222d3e8c4e25b02bf791b65d_xxhdpi" alt="Second slide" />
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-84354b31222d3e8c4e25b02bf791b65d_xxhdpi" alt="Third slide" />
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExample" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExample" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-12">
            <img class="d-block w-100 p-1" src="https://cf.shopee.vn/file/vn-50009109-47e052a0ca43437170502077d43cc088_xhdpi" alt="First slide" />
            <img class="d-block w-100 p-1 pt-0" src="https://cf.shopee.vn/file/vn-50009109-ac3fd43493c8c25669bd43bcfb600b81_xhdpi" alt="First slide" />
        </div>
    </div>
    <div class="col-md-9">
        <div class="post-list mt-3">
            <h2 class="text-center">Tin tức</h2>
            <div class="row">
                @foreach($posts as $post)
                    <div class="card col-4 m-2 p-0" style="width: 18rem;">
                        <a href="{{ route('home.post.detail', ['slug' => $post->slug])}}">
                            <img width="400" height="253" class="card-img-top" src="{{ $post->image }}" alt="{{ $post->title }}">
                            <div class="card-body">
                                <h5 class="card-title detail-post">{{ $post->title }}</h5>
                                <a href="{{ route('home.post.detail', ['slug' => $post->slug])}}" class="btn btn-primary">Xem thêm</a>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="product-similar mt-3">
            <h2 class="text-center">Sản phẩm</h2>
            @foreach($products as $product)
            <div class="col item mb-2">
                <a href="{{ route('home.product', ['slug' => $product->slug]) }}">
                    <div class="card h-100">
                        <div class="sale p-1">
                            <span class="price-sale">Giảm @money_vn($product->price * ($product->sale/100))</span>
                            <img style="background-image: url({{$product->thumbnail}});" class="card-img-top" src="{{ asset('assets/img/user/sale.png') }}" alt="{{ $product->name }}" />
                        </div>
                        <div class="card-body pt-1">
                            <a href="{{ route('home.product', ['slug' => $product->slug]) }}">
                                <span class="card-title fs-16 fw-500">{{ $product->name }}</span>
                            </a>
                            <div class="price d-flex justify-content-between pt-1 align-items-center">
                                <span class="price-new fs-14">@money_vn($product->price * ((100 - $product->sale)/100))</span>
                                <span class="price-old fs-14">@money_vn($product->price)</span>
                            </div>
                            <div class="pt-1">
                                @php $rating = $product->point; @endphp 
                                @foreach(range(1,5) as $i)
                                    <span class="fa-stack" style="width:1em">
                                        <i class="far fa-star fa-stack-1x"></i>
                                        @if($rating >0)
                                            @if($rating >0.5)
                                                <i class="fas fa-star fa-stack-1x"></i>
                                            @else
                                                <i class="fas fa-star-half fa-stack-1x"></i>
                                            @endif
                                        @endif
                                        @php $rating--; @endphp
                                    </span>
                                @endforeach
                            </div>
                            <span class="fs-14 pt-2 d-flex">Giảm thêm 5% khi thanh toán online</span>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        
    </div>
</div>
@endsection