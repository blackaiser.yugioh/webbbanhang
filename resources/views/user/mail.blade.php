<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
    <div style="max-width:620px;min-width:320px;margin:30px auto;font-family:Helvetica,sans-serif;padding:30px;background:white;color:#424242;border:2px solid #f9f9f9;">
        <p style="text-align:center">
            <a  title="logo" target="_blank">
                <img src="http://localhost:8000/assets/img/favicon/logo.png" style="width:250px" class="CToWUd">
            </a>
        </p>
        <div style="border:1px solid #f9f9f9;margin:15px auto 8px auto"></div>
        <br>

        <div style="">
            <div>
                <p>Xin chào,</p>
                <p>Mã xác thực của bạn là.</p>
                <p style="text-align: center; font-size: 20px; font-weight: bold; background-color: #ec0867; color: white">{{$otp}}</p>
                <p>Mã xác thực chỉ có hiệu lực trong vòng <strong>3 phút</strong>. Vui lòng không chia sẻ mã này cho người khác.</p>
                <p><strong>Lưu ý:</strong> Nếu bạn không thực hiện đăng ký nhưng vẫn nhận được email này, xin vui lòng bỏ qua email và nên thay đổi mật khẩu cho tài khoản ngay (nếu có).</p>
                <p>Trân trọng</p>
            </div>        
        </div>

        <div style="border:1px solid #f9f9f9;margin:15px auto 8px auto"></div>
        <br>
        <p>Nếu có vấn đề gì, quý khách vui lòng liên hệ theo số điện thoại <a href="tel:0123456677">0123456677</a> để được hỗ trợ nhanh nhất.</p>
        <div style="border:1px solid #f9f9f9;margin:15px auto 8px auto"></div>
        <br>

        
    </div>
</body>

</html>
