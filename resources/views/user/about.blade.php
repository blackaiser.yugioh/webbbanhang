@extends('layouts/layout-user' )
@section('title', 'Giới thiệu')
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="pt-3 d-flex">
        <div id="carouselExample" class="carousel slide col-lg--8 col-md-8 col-12 py-1" data-bs-ride="carousel">
            <ol class="carousel-indicators">
                <li data-bs-target="#carouselExample" data-bs-slide-to="0" class="active"></li>
                <li data-bs-target="#carouselExample" data-bs-slide-to="1"></li>
                <li data-bs-target="#carouselExample" data-bs-slide-to="2"></li>
            </ol> 
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-84354b31222d3e8c4e25b02bf791b65d_xxhdpi" alt="First slide" />
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-84354b31222d3e8c4e25b02bf791b65d_xxhdpi" alt="Second slide" />
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-84354b31222d3e8c4e25b02bf791b65d_xxhdpi" alt="Third slide" />
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExample" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExample" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-12">
            <img class="d-block w-100 p-1" src="https://cf.shopee.vn/file/vn-50009109-47e052a0ca43437170502077d43cc088_xhdpi" alt="First slide" />
            <img class="d-block w-100 p-1 pt-0" src="https://cf.shopee.vn/file/vn-50009109-ac3fd43493c8c25669bd43bcfb600b81_xhdpi" alt="First slide" />
        </div>
    </div>
</div>
            <section class="aboutTop">
                <div style="margin-top: 15px" class="container">
                    <h3>Khánh store Xin kính chào các bạn</h3>
                    <p>Chào mừng các bạn đến với cửa hàng khánh store</p>
                    <p style="margin-bottom: 25px">Khanhstore là một cửa hàng  ở thành phố Nam Định, tỉnh Nam Định chuyên cung cấp  và bán các sản phẩm công nghệ phục vụ nhu cầu của quý khách hàng.</p>
                    <p>Với tiêu chí “ Đặt chất lượng lên hàng đầu ” , chúng tối cam kết sẽ mang lại những trải nghiệm tốt nhất , cũng như chất lượng tốt nhất cho quý khách hàng.</p>
                    <p> </p>
                </div>
            </section>
@endsection