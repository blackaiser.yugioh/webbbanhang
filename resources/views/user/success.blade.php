@extends('layouts/layout-user' )
@section('title', 'Thông tin đơn hàng')
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')
<div class="cart">
    <success 
        :data="{{ json_encode($data) }}">
    </success>
</div>
@endsection