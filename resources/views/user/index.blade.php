@extends('layouts/layout-user' )
@section('title', 'Trang chủ')
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')

<div class="pt-3 d-flex">
    <div id="carouselExample" class="carousel slide col-lg--8 col-md-8 col-12 py-1" data-bs-ride="carousel">
        <ol class="carousel-indicators">
            <li data-bs-target="#carouselExample" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#carouselExample" data-bs-slide-to="1"></li>
            <li data-bs-target="#carouselExample" data-bs-slide-to="2"></li>
        </ol> 
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-84354b31222d3e8c4e25b02bf791b65d_xxhdpi" alt="First slide" />
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-ef4d8de12139c1028826ab88d4c24cea_xxhdpi" alt="Second slide" />
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://cf.shopee.vn/file/vn-50009109-8a1f2cc3fff8b6fa545c15ea2c23e861_xxhdpi" alt="Third slide" />
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExample" role="button" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExample" role="button" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-12">
        <img class="d-block w-100 p-1" src="https://cf.shopee.vn/file/vn-50009109-47e052a0ca43437170502077d43cc088_xhdpi" alt="First slide" />
        <img class="d-block w-100 p-1 pt-0" src="https://cf.shopee.vn/file/vn-50009109-ac3fd43493c8c25669bd43bcfb600b81_xhdpi" alt="First slide" />
    </div>
</div>
<div class="category bg-white mt-3">
    <div class="border d-none p-3 fs-18 fw-500 cat-title">Danh mục</div>
    <div class="d-flex">
        @foreach($cats as $cat)
            <a href="<?php echo('/danh-sach-san-pham?cat=' .$cat->id) ?>" class="col-md-2 col-lg-2 col-4 d-gird bg-white cat-hover justify-content-center p-4">
                <img src="{{ $cat->thumbnail }}" class="border-radius-50" height="100" width="100" alt="">
                <span style="color: #566a7f" class="text-center fs-18 fw-500">{{ $cat->name }}</span>
            </a>
        @endforeach
    </div>
</div>
<div class="banner-1 d-flex justify-content-center py-4">
    <img class="w-100" src="https://images.fpt.shop/unsafe/fit-in/1200x100/filters:quality(90):fill(white)/fptshop.com.vn/Uploads/Originals/2023/7/4/638240666352557412_H7_1200x100.png" alt="">
</div>
<div class="product-sale bg-white p-2">
    <div class="product-sale-title d-flex align-items-center">
        <img src="{{ asset('assets/img/user/hot-sale.png') }}" height="70px" alt="">
        <h3 class="title-hot-sale my-auto">Khuyến mại hot</h3>
    </div>
    <div id="news-slider" class="owl-carousel row-cols-5">
        @foreach($sales as $product)
            <div class="col item">
                <a href="{{ route('home.product', ['slug' => $product->slug]) }}">
                    <div class="card h-100">
                        <div class="sale p-1">
                            <span class="price-sale">Giảm @money_vn($product->price * ($product->sale/100))</span>
                            <img style="background-image: url({{$product->thumbnail}});" class="card-img-top" src="{{ asset('assets/img/user/sale.png') }}" alt="{{ $product->name }}" />
                        </div>
                        <div class="card-body pt-1">
                            <a href="{{ route('home.product', ['slug' => $product->slug]) }}">
                                <span class="card-title fs-16 fw-500">{{ $product->name }}</span>
                            </a>
                            <div class="price d-flex justify-content-between pt-1 align-items-center">
                                <span class="price-new fs-14">@money_vn($product->price * ((100 - $product->sale)/100))</span>
                                <span class="price-old fs-14">@money_vn($product->price)</span>
                            </div>
                            <div class="pt-1">
                                @php $rating = $product->point; @endphp 
                                @foreach(range(1,5) as $i)
                                    <span class="fa-stack" style="width:1em">
                                        <i class="far fa-star fa-stack-1x"></i>
                                        @if($rating >0)
                                            @if($rating >0.5)
                                                <i class="fas fa-star fa-stack-1x"></i>
                                            @else
                                                <i class="fas fa-star-half fa-stack-1x"></i>
                                            @endif
                                        @endif
                                        @php $rating--; @endphp
                                    </span>
                                @endforeach
                            </div>
                            <span class="fs-14 pt-2 d-flex">Giảm thêm 5% khi thanh toán online</span>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>
<div class="banner-1 d-flex justify-content-center py-4">
    <img class="w-100" src="assets/img/user/banner-1.webp" alt="">
</div>
<div class="product-sale bg-white p-2">
    <div class="product-sale-title d-flex align-items-center p-3">
        <h3 class="title-hot-sale my-auto">Top bán chạy</h3>
    </div>
    <div class="product-list-index row row-cols-5">
        @foreach($bestseller as $seller)
            <div class="col item">
                <a href="{{ route('home.product', ['slug' => $seller->slug]) }}">
                    <div class="card h-100">
                        <div class="sale p-1">
                            <img class="card-img-top" src="{{ $seller->thumbnail }}" alt="{{ $seller->name }}" />
                        </div>
                        <div class="card-body pt-1">
                            <a href="{{ route('home.product', ['slug' => $seller->slug]) }}">
                                <span class="card-title fs-16 fw-500">
                                    {{ $seller->name }}
                                </span>
                            </a>
                            <div class="price d-flex justify-content-between pt-1 align-items-center">
                                @if($seller->sale > 0)
                                    <span class="price-new fs-14">@money_vn($seller->price * ((100 - $seller->sale)/100))</span>
                                    <span class="price-old fs-14">@money_vn($seller->price)</span>
                                @else
                                    <span class="price-new fs-14">@money_vn($seller->price)</span>
                                @endif
                            </div>
                            <div class="pt-1">
                                @php $rating = $seller->point; @endphp 
                                @foreach(range(1,5) as $i)
                                    <span class="fa-stack" style="width:1em">
                                        <i class="far fa-star fa-stack-1x"></i>
                                        @if($rating >0)
                                            @if($rating >0.5)
                                                <i class="fas fa-star fa-stack-1x"></i>
                                            @else
                                                <i class="fas fa-star-half fa-stack-1x"></i>
                                            @endif
                                        @endif
                                        @php $rating--; @endphp
                                    </span>
                                @endforeach
                            </div>
                            <span class="fs-14 pt-2 d-flex">Giảm thêm 5% khi thanh toán online</span>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>

@endsection
@section('page-script')
<script>
    $(document).ready(function () {
        $("#news-slider").owlCarousel({
            items: 5,
            autoPlay: 1000,
            itemsDesktop: 4,
            itemsDesktopSmall: 3,
            itemsMobile: 1,
            pagination: true,
            autoPlay: true ,
            navigation : false,
            navigationText : ["prev","next"],
        });
        $("#bestseller").owlCarousel({
            items: 5,
            autoPlay: 1000,
            itemsDesktop: 4,
            itemsDesktopSmall: 3,
            itemsMobile: 1,
            pagination: true,
            autoPlay: true ,
            navigation : false,
            navigationText : ["prev","next"],
        });
    });
</script>
@endsection