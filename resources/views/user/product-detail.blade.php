@extends('layouts/layout-user' )
@section('title', $product->name)
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')
<div class="product-detail">
    <detail-product 
        :star="{{ json_encode($listStar) }}"
        :samilar="{{ json_encode($productSamilar) }}"
        :product="{{ json_encode($product) }}">
    </detail-product>
</div>
@endsection