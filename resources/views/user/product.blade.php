@extends('layouts/layout-user' )
@section('title', 'Trang chủ')
@section('vendor-script')
<script src="{{asset('assets/vendor/libs/masonry/masonry.js')}}"></script>
@endsection
@section('content')

<product-list></product-list>

@endsection
@section('page-script')
<script>
    var mtarget = $('.jq-list-link');
if (mtarget.length) {   
    mtarget.click(function(e) {

        if ( $(this).parent().attr('aria-expanded') == "true") {                    
        console.log('aria true');
        window.location = $(this).href; 
        } else if ($(this).parent().attr('aria-expanded') == "false") {
        console.log('aria false');
        e.preventDefault();
        } else {
        console.log('aria does not exist');
        e.preventDefault();
        }                                
    });
  };
    $(document).ready(function () {
        $("#news-slider").owlCarousel({
            items: 5,
            autoPlay: 1000,
            itemsDesktop: 4,
            itemsDesktopSmall: 3,
            itemsMobile: 1,
            pagination: true,
            autoPlay: true ,
            navigation : false,
            navigationText : ["prev","next"],
        });
        $("#bestseller").owlCarousel({
            items: 5,
            autoPlay: 1000,
            itemsDesktop: 4,
            itemsDesktopSmall: 3,
            itemsMobile: 1,
            pagination: true,
            autoPlay: true ,
            navigation : false,
            navigationText : ["prev","next"],
        });
    });
</script>
@endsection