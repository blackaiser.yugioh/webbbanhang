<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ReviewController;
use App\Http\Controllers\Admin\VoucherController;
use App\Http\Controllers\User\AddressController;
use App\Http\Controllers\User\CartController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\InfoController;
use App\Http\Controllers\User\OrderController;
use App\Http\Controllers\User\PostController as UserPostController;
use App\Http\Controllers\User\ProductController as UserProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$controller_path = 'App\Http\Controllers';

// authentication
Route::get('/sign-in',[UserController::class,'login'])->name('login');
Route::get('/forgot-password',[UserController::class,'forgot'])->name('forgot');
Route::post('/sendOTP',[UserController::class,'sendOTP'])->name('sendOTP');
Route::post('/confirmOtp',[UserController::class,'confirmOtp'])->name('confirmOtp');
Route::post('/updatePass',[UserController::class,'updatePass'])->name('updatePass');
Route::post('/post-sign-in',[UserController::class,'signIn'])->name('signIn');
Route::get('/register',[UserController::class,'register'])->name('sign-up');
Route::post('/post-register',[UserController::class,'postRegister'])->name('postRegister');
Route::get('/sign-out',[UserController::class,'signOut'])->name('sign-out');

//admin
Route::group(['middleware' => ['role:super_admin', 'auth'],'prefix' => 'admin', 'namespace' => 'Admin'], function(){
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard/{date}', [DashboardController::class, 'detail'])->name('dashboard.detail');

    Route::group(['prefix' => 'category'],function() {
        Route::get('/', [CategoryController::class, 'index'])->name('category');
        Route::get('/{id}', [CategoryController::class, 'edit'])->name('category.edit');
        Route::get('/create', [CategoryController::class, 'create'])->name('category-create');
        Route::post('/store', [CategoryController::class, 'store'])->name('category-store');
        Route::post('/update/{id}', [CategoryController::class, 'update'])->name('category.update');
        Route::get('/hide/{id}', [CategoryController::class, 'hide'])->name('category.hide');
    });
    Route::group(['prefix' => 'product'],function() {
        Route::get('/', [ProductController::class, 'index'])->name('product');
        Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
        Route::get('/create', [ProductController::class, 'create'])->name('product.create');
        Route::post('/store', [ProductController::class, 'store'])->name('product.store');
        Route::post('/update/{id}', [ProductController::class, 'update'])->name('product.update');
        Route::get('/hide/{id}', [ProductController::class, 'hide'])->name('product.hide');
        Route::get('/review', [ReviewController::class, 'index'])->name('product.review');
        Route::get('/hide-review/{id}', [ReviewController::class, 'update'])->name('product.hide-review');
        Route::get('/image/{id?}', [ProductController::class, 'image'])->name('product.image');

    });
    Route::group(['prefix' => 'order'],function() {
        Route::get('/', [AdminOrderController::class, 'index'])->name('order');
        Route::post('/update', [AdminOrderController::class, 'update'])->name('order.update');
        Route::get('/detail/{id}', [AdminOrderController::class, 'show'])->name('order.show');
        // Route::get('/create', [ProductController::class, 'create'])->name('product.create');
        // Route::post('/store', [ProductController::class, 'store'])->name('product.store');
        // Route::post('/update/{id}', [ProductController::class, 'update'])->name('product.update');
        // Route::get('/hide/{id}', [ProductController::class, 'hide'])->name('product.hide');
        // Route::get('/review', [ReviewController::class, 'index'])->name('product.review');
        // Route::get('/hide-review/{id}', [ReviewController::class, 'update'])->name('product.hide-review');
    });
    Route::group(['prefix' => 'user'], function() {
       Route::get('/', [UserController::class, 'index'])->name('user'); 
       Route::get('lock/{id}', [UserController::class, 'lock'])->name('user.lock'); 
    });
    Route::group(['prefix' => 'voucher'],function() {
        Route::get('/', [VoucherController::class, 'index'])->name('voucher');
        Route::get('/edit/{id}', [VoucherController::class, 'edit'])->name('voucher.edit');
        Route::get('/create', [VoucherController::class, 'create'])->name('voucher.create');
        Route::post('/store', [VoucherController::class, 'store'])->name('voucher.store');
        Route::post('/update/{id}', [VoucherController::class, 'update'])->name('voucher.update');
        Route::get('/hide/{id}', [VoucherController::class, 'hide'])->name('voucher.hide');
    });

    Route::group(['prefix' => 'contact'],function() {
        Route::get('/', [ContactController::class, 'index'])->name('contact');
    });

    Route::group(['prefix' => 'post'],function() {
        Route::get('/', [PostController::class, 'index'])->name('post');
        Route::get('/create', [PostController::class, 'create'])->name('post.create');
        Route::post('/store', [PostController::class, 'store'])->name('post.store');
        Route::get('/edit/{id}', [PostController::class, 'edit'])->name('post.edit');
        Route::post('/update/{id}', [PostController::class, 'update'])->name('post.update');
        Route::get('/hide/{id}', [PostController::class, 'hide'])->name('post.hide');
    });
});

// Main
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/san-pham/{slug}', [HomeController::class, 'detailProduct'])->name('home.product');
Route::post('/review', [HomeController::class, 'review'])->name('home.review');
Route::get('/about', [HomeController::class, 'about'])->name('home.about');
Route::get('/tin-tuc', [UserPostController::class, 'index'])->name('home.post');
Route::get('/tin-tuc/{slug}', [UserPostController::class, 'show'])->name('home.post.detail');
Route::get('/list-review/{id}', [HomeController::class, 'getReview'])->name('home.get.review');
Route::post('/addToCart', [CartController::class, 'addToCart'])->name('home.add-to-cart');
Route::get('/list-city', [AddressController::class, 'listCity'])->name('home.list-city');
Route::post('/list-district', [AddressController::class, 'listDistrict'])->name('home.list-district');
Route::post('/list-area', [AddressController::class, 'listArea'])->name('home.list-area');
Route::get('/danh-sach-san-pham', [UserProductController::class, 'index'])->name('home.list-product');
Route::post('/product-list', [UserProductController::class, 'getList'])->name('getList');
Route::get('/category-list', [UserProductController::class, 'category'])->name('product.category');
Route::get('/lien-he', [HomeController::class, 'contact'])->name('home.contact');
Route::post('/gui-lien-he', [HomeController::class, 'sendContact'])->name('home.sendContact');

//Main auth
Route::group(['middleware' => ['auth']], function(){
    Route::get('/cart', [CartController::class, 'index'])->name('home.cart');
    Route::post('/delete-cart', [CartController::class, 'destroy'])->name('cart.delete');
    Route::post('/post-checkout', [OrderController::class, 'create'])->name('order.index');
    Route::get('/checkout', [OrderController::class, 'index'])->name('order.index');
    Route::get('/chi-tiet-don-hang', [OrderController::class, 'returnUrl'])->name('chi-tiet-don-hang');
    Route::post('/payment', [OrderController::class, 'confirmPayment'])->name('confirm');
    Route::get('/thong-tin-ca-nhan', [InfoController::class, 'info'])->name('info');
    Route::get('/don-hang', [InfoController::class, 'listOrder'])->name('listOrder');
    Route::post('/update-user', [InfoController::class, 'updateUser'])->name('updateUser');
    Route::post('/change-password', [InfoController::class, 'changePassword'])->name('changePassword');
    Route::get('/cancel-order/{id}', [InfoController::class, 'cancelOrder'])->name('cancelOrder');
});




// Route::get('/dashboard', $controller_path . '\dashboard\Analytics@index')->name('dashboard-analytics');

// layout
Route::get('/layouts/without-menu', $controller_path . '\layouts\WithoutMenu@index')->name('layouts-without-menu');
Route::get('/layouts/without-navbar', $controller_path . '\layouts\WithoutNavbar@index')->name('layouts-without-navbar');
Route::get('/layouts/fluid', $controller_path . '\layouts\Fluid@index')->name('layouts-fluid');
Route::get('/layouts/container', $controller_path . '\layouts\Container@index')->name('layouts-container');
Route::get('/layouts/blank', $controller_path . '\layouts\Blank@index')->name('layouts-blank');

// pages
Route::get('/pages/account-settings-account', $controller_path . '\pages\AccountSettingsAccount@index')->name('pages-account-settings-account');
Route::get('/pages/account-settings-notifications', $controller_path . '\pages\AccountSettingsNotifications@index')->name('pages-account-settings-notifications');
Route::get('/pages/account-settings-connections', $controller_path . '\pages\AccountSettingsConnections@index')->name('pages-account-settings-connections');
Route::get('/pages/misc-error', $controller_path . '\pages\MiscError@index')->name('pages-misc-error');
Route::get('/pages/misc-under-maintenance', $controller_path . '\pages\MiscUnderMaintenance@index')->name('pages-misc-under-maintenance');

// authentication
Route::get('/auth/login-basic', $controller_path . '\authentications\LoginBasic@index')->name('auth-login-basic');
Route::get('/auth/register-basic', $controller_path . '\authentications\RegisterBasic@index')->name('auth-register-basic');
Route::get('/auth/forgot-password-basic', $controller_path . '\authentications\ForgotPasswordBasic@index')->name('auth-reset-password-basic');

// cards
Route::get('/cards/basic', $controller_path . '\cards\CardBasic@index')->name('cards-basic');

// User Interface
Route::get('/ui/accordion', $controller_path . '\user_interface\Accordion@index')->name('ui-accordion');
Route::get('/ui/alerts', $controller_path . '\user_interface\Alerts@index')->name('ui-alerts');
Route::get('/ui/badges', $controller_path . '\user_interface\Badges@index')->name('ui-badges');
Route::get('/ui/buttons', $controller_path . '\user_interface\Buttons@index')->name('ui-buttons');
Route::get('/ui/carousel', $controller_path . '\user_interface\Carousel@index')->name('ui-carousel');
Route::get('/ui/collapse', $controller_path . '\user_interface\Collapse@index')->name('ui-collapse');
Route::get('/ui/dropdowns', $controller_path . '\user_interface\Dropdowns@index')->name('ui-dropdowns');
Route::get('/ui/footer', $controller_path . '\user_interface\Footer@index')->name('ui-footer');
Route::get('/ui/list-groups', $controller_path . '\user_interface\ListGroups@index')->name('ui-list-groups');
Route::get('/ui/modals', $controller_path . '\user_interface\Modals@index')->name('ui-modals');
Route::get('/ui/navbar', $controller_path . '\user_interface\Navbar@index')->name('ui-navbar');
Route::get('/ui/offcanvas', $controller_path . '\user_interface\Offcanvas@index')->name('ui-offcanvas');
Route::get('/ui/pagination-breadcrumbs', $controller_path . '\user_interface\PaginationBreadcrumbs@index')->name('ui-pagination-breadcrumbs');
Route::get('/ui/progress', $controller_path . '\user_interface\Progress@index')->name('ui-progress');
Route::get('/ui/spinners', $controller_path . '\user_interface\Spinners@index')->name('ui-spinners');
Route::get('/ui/tabs-pills', $controller_path . '\user_interface\TabsPills@index')->name('ui-tabs-pills');
Route::get('/ui/toasts', $controller_path . '\user_interface\Toasts@index')->name('ui-toasts');
Route::get('/ui/tooltips-popovers', $controller_path . '\user_interface\TooltipsPopovers@index')->name('ui-tooltips-popovers');
Route::get('/ui/typography', $controller_path . '\user_interface\Typography@index')->name('ui-typography');

// extended ui
Route::get('/extended/ui-perfect-scrollbar', $controller_path . '\extended_ui\PerfectScrollbar@index')->name('extended-ui-perfect-scrollbar');
Route::get('/extended/ui-text-divider', $controller_path . '\extended_ui\TextDivider@index')->name('extended-ui-text-divider');

// icons
Route::get('/icons/boxicons', $controller_path . '\icons\Boxicons@index')->name('icons-boxicons');

// form elements
Route::get('/forms/basic-inputs', $controller_path . '\form_elements\BasicInput@index')->name('forms-basic-inputs');
Route::get('/forms/input-groups', $controller_path . '\form_elements\InputGroups@index')->name('forms-input-groups');

// form layouts
Route::get('/form/layouts-vertical', $controller_path . '\form_layouts\VerticalForm@index')->name('form-layouts-vertical');
Route::get('/form/layouts-horizontal', $controller_path . '\form_layouts\HorizontalForm@index')->name('form-layouts-horizontal');

// tables
Route::get('/tables/basic', $controller_path . '\tables\Basic@index')->name('tables-basic');
